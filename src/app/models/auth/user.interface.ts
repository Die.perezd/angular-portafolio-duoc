export type Roles = "EMPLEADO" | "ADMIN";

export interface User{
    username: string;
    password: string;
}

export interface UserResponse{
    access_token: string;
    message: string;
    nombres: string;
    primer_apellido:string;
    unidad:string;
    rol:string;
    area:string;
    id_empresa:string,
    nombre_empresa:string,
    id_empleado_empresa:string
}