export interface TareasFlujosDeTrabajoI{
    id_tarea_flujo:BigInteger,
    id_flujo:BigInteger,
    id_tarea:BigInteger,
    plazo:string
}