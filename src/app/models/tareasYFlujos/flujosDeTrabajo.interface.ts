export interface FlujosDeTrabajoI{
    id_flujo_trabajo:BigInteger,
    id_empresa:string,
    nombre_flujo_trabajo:string,
    descripcion_flujo_trabajo:string
}

export interface TareaFlujoI{
    id_tarea_flujo:number,
    id_flujo_trabajo:number,
    id_tarea:number,
    fecha_asignacion:Date
}

export interface FlujoEmpleadoI{
    id_tarea_flujo_empleado:number,
    fecha_plazo:Date,
    id_empleado_responsable:number,
    id_empleado_jefatura:number
    id_tarea_flujo:number,
    fecha_asignacion:Date,
    id_estado_tarea_flujo_empleado:number
}