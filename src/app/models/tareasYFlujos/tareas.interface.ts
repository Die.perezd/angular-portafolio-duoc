export interface TareasI{
    id_tarea:string,
    nombre_tarea:string,
    descripcion_tarea:string
}

export interface MisTareasI{
    id_tarea_empleado:BigInteger,
    fecha_plazo:Date,
    fecha_asignacion:Date,
    id_tarea:BigInteger,
    id_empleado_responsable:BigInteger,
    id_empleado_jefatura:BigInteger,
    id_estado_tarea:BigInteger,
    nombre_tarea:string,
    nombre_estado:string
}