export interface MisTareasI{
    id_tarea_empleado_empresa:BigInteger,
    id_empleado_empresa:BigInteger,
    id_empleado_subordinador:BigInteger,
    id_tarea_flujo:BigInteger,
    id_estado_tarea:number,
    fecha_asignacion:Date,
    fecha_inicio:Date,
    fecha_termino:Date
}