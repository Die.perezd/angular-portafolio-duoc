export interface DependenciaEmpleadoI{
    id_dependencia:BigInteger,
    id_empleado_jefe:BigInteger,
    id_empleado_subordinado:BigInteger,
    id_empleado_empresa:BigInteger,
}