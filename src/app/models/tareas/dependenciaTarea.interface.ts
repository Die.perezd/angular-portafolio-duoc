export interface DependenciaTareaI{
    id_tarea_empleado:BigInteger,
    fecha_plazo:Date,
    fecha_asignacion:Date,
    id_tarea:BigInteger,
    id_empleado_responsable:BigInteger,
    id_empleado_jefatura:BigInteger,
    id_estado_tarea:string,
    nombre_tarea:string,
    id_estado_tarea_flujo_empleado:number
}

export interface SubTareaI{
    id_subtarea:BigInteger,
    nombre_subtarea:string,
    descripcion_subtarea:string,
    fecha_plazo:Date,
    id_tarea_empleado:BigInteger,
    id_empleado_responsable:BigInteger,
    id_estado_subtarea:BigInteger
}

