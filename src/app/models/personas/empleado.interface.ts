export interface EmpleadosI{
    nombres:String,
    primer_apellido:String,
    segundo_apellido:String,
    numero_documento:String,
    fecha_nacimiento:String,
    direccion:String,
    email:String
}

export interface EmpleadosIDI{
    id_empleado:number,
    nombres:String,
    primer_apellido:String,
    segundo_apellido:String,
    numero_documento:String,
    fecha_nacimiento:String,
    direccion:String,
    email:String
}