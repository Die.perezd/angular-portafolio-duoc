export interface EmpleadosEmpresaI{
    id_empleado_empresa:BigInteger,
    id_empresa:string,
    id_rol:string,
    id_unidad:string,
    id_area:string,
    username:string,
    pass:string,
    nombres:String,
    primer_apellido:String,
    segundo_apellido:String,
    numero_documento:String,
    fecha_nacimiento:String,
    direccion:String,
    email:string,
    nombre_empresa:string
}

export interface EmpleadoJefeI{
    id_empleado_empresa:BigInteger,
    id_empresa:string,
    id_rol:string,
    id_unidad:string,
    id_area:string,
    username:string,
    pass:string,
    nombres:String,
    primer_apellido:String,
    segundo_apellido:String,
    numero_documento:String,
    fecha_nacimiento:String,
    direccion:String,
    email:string,
    nombre_empresa:string,
    id_empleado_jefe:string,
    id_empleado_subordinado:string
}
