export interface AreasI{
    id_area:BigInteger,
    nombre_area:string,
    descripcion_area:string,
    id_empresa:string
}