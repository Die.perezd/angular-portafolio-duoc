import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr'

import Swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class AlertasService {

  constructor(private toast: ToastrService, public router:Router) { }

  showSuccess(texto: string | undefined, titulo: string | undefined) {
    this.toast.success(texto, titulo)
  }

  showEdit() {
    Swal.fire(
      'Editado!',
      'Tu registrado fue editado exitosamente!',
      'success'
    )
  }

  showDelete(texto: string | undefined, titulo: string | undefined) {
    this.toast.error(texto)
  }

  ShowSucces2() {
    Swal.fire(
      'Creado!',
      'Tu registrado fue creado exitosamente!',
      'success'
    )
  }

  showFormError(message: string | undefined) {
    Swal.fire({
      title: "Error",
      html: message,
      icon: 'warning'
    })
  }

  errorLogin() {
    Swal.fire(
      'Error',
      'Usuario y/o contraseña incorrectos',
      'error'
    )
  }

  errorPermisos() {
    Swal.fire(
      'Error',
      'No cuenta con los permisos para acceder a esta funcionalidad',
      'error'
    ).then((result) => {
      this.router.navigate(['/principal'])
    })
  }

}
