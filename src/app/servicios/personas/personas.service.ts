import { Injectable } from '@angular/core';

//importamos httpClient
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { EmpleadosI } from 'src/app/models/personas/empleado.interface';
import { Observable, Subject } from 'rxjs';

import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PersonasService {

  // Creamos una url variable para empresas
  urlListarEmpleados = '/api/?r=apis/empleado'
  urlUpdateEmpleados = '/api/?r=apis/empleado/update'
  urlCreateEmpleados = '/api/?r=apis/empleado/create'
  urlDeleteEmpleados = '/api/?r=apis/empleado/delete'
  urlViewEmpleados = '/api/?r=apis/empleado/view'

  constructor(private http: HttpClient) {
    console.log('Probando servicio para obtener empleados')
  }

  // PRUEBAAAAA
  private _refresh = new Subject<void>()
  get refresh() {
    return this._refresh
  }

  // PRUEBA COPIA ARRAY
  

  getEmpleados() {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')
    return this.http.get(this.urlListarEmpleados, {
      headers: headers
    })
  }

  getSingleEmpleado(id: string | null): Observable<EmpleadosI> {
    let direccion = this.urlViewEmpleados + '&id=' + id
    return this.http.get<EmpleadosI>(direccion)
  }

  createEmpleado(form: EmpleadosI): Observable<EmpleadosI> {
    let direccion = this.urlCreateEmpleados
    return this.http.post<EmpleadosI>(direccion, form).pipe(
      tap(() => {
        this._refresh.next();
      })
    )
  }

  putEmpleado(form: EmpleadosI, id: string | null): Observable<EmpleadosI> {
    let direccion = this.urlUpdateEmpleados + '&id=' + id;
    return this.http.put<EmpleadosI>(direccion, form)

  }

  deleteEmpleado(id: string | null): Observable<EmpleadosI> {
    let direccion = this.urlDeleteEmpleados + '&id=' + id;
    let Options = {
      headers: new HttpHeaders({
        'Content-type': 'aplication/json'
      }),
    }
    return this.http.delete<EmpleadosI>(direccion, Options)
  }



}
