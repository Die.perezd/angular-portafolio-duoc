import { Injectable } from '@angular/core';

//importamos httpClient
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable, Subject } from 'rxjs';
import { MisTareasI, TareasI } from 'src/app/models/tareasYFlujos/tareas.interface';
import { tap } from 'rxjs/operators';
import { EstadosTareasI } from 'src/app/models/tareasYFlujos/estadoTareas.interface';
import { FlujoEmpleadoI, FlujosDeTrabajoI, TareaFlujoI } from 'src/app/models/tareasYFlujos/flujosDeTrabajo.interface';
import { TareasFlujosDeTrabajoI } from 'src/app/models/tareasYFlujos/tareasFlujodeTrabajo.interface';
import { DependenciaEmpleadoI } from 'src/app/models/tareas/dependencia.interface';
import { DependenciaTareaI, SubTareaI } from 'src/app/models/tareas/dependenciaTarea.interface';

@Injectable({
  providedIn: 'root'
})
export class TareasYFlujosService {

  // Creamos una url variable para empresas
  urlListarTareas = '/api/?r=apis/tarea/list&id_empresa='
  urlUpdateTareas = '/api/?r=apis/tarea/update'
  urlCreateTareas = '/api/?r=apis/tarea/create'
  urlDeleteTareas = '/api/?r=apis/tarea/delete'
  urlViewTareas = '/api/?r=apis/tarea/view'

  //----------------------------------------------------

  // Creamos una url variable para estados
  urlListarEstadoTareas = '/api/?r=apis/estado-tarea'
  urlUpdateEstadoTareas = '/api/?r=apis/estado-tarea/update&id='
  urlCreateEstadoTareas = '/api/?r=apis/estado-tarea/create'
  urlDeleteEstadoTareas = '/api/?r=apis/estado-tarea/delete'
  urlViewEstadoTareas = '/api/?r=apis/estado-tarea/view'

  //----------------------------------------------------

  // Creamos una url variable para flujos
  urlListarFlujosDeTrabajo = '/api/?r=apis/flujo-trabajo/list&id_empresa='
  urlUpdateFlujosDeTrabajo = '/api/?r=apis/flujo-trabajo/update'
  urlCreateFlujosDeTrabajo = '/api/?r=apis/flujo-trabajo/create'
  urlDeleteFlujosDeTrabajo = '/api/?r=apis/flujo-trabajo/delete'
  urlViewFlujosDeTrabajo = '/api/?r=apis/flujo-trabajo/view'

  // TAREA FLUJO
  urlAddTareaFlujo = '/api/?r=apis/tarea-flujo/create'
  urlListarTareaFlujo = '/api/?r=apis/tarea-flujo/list-tareas-flujo&id_flujo_trabajo='
  urlAsignarFlujo = '/api/?r=apis/tarea-flujo-empleado/create'

  urlMisFlujos = '/api/?r=apis/tarea-flujo-empleado/listar-flujos-trabajo-empleado&id_empleado_responsable='
  urlTareasMiFlujo = '/api/?r=apis/tarea-flujo-empleado/listar-tareas-flujo-empleado&fecha_asignacion_flujo='
  urlUpdateTareaMiFlujo = '/api/?r=apis/tarea-flujo-empleado/update&id='

  urldashboard = '/api/?r=apis/dashboard/tareas-por-mes-asignacion'

  //----------------------------------------------------

  // Creamos una url variable para tareas flujo
  urlListarTareasFlujosDeTrabajo = '/api/?r=apis/tareas-flujo'
  urlUpdateTareasFlujosDeTrabajo = '/api/?r=apis/tareas-flujo/update'
  urlCreateTareasFlujosDeTrabajo = '/api/?r=apis/tareas-flujo/create'
  urlDeleteTareasFlujosDeTrabajo = '/api/?r=apis/tareas-flujo/delete'
  urlViewTareasFlujosDeTrabajo = '/api/?r=apis/tareas-flujo/view'

  //----------------------------------------------------

  // Creamos una url variable para tareas flujo
  urlObtenerTareasEmpleado = '/api/?r=apis/tareas-empleado'
  urlUpdateTareasEmpleado = '/api/?r=apis/tareas-flujo/update'
  urlCreateTareasEmpleado = '/api/?r=apis/tareas-flujo/create'
  urlDeleteTareasEmpleado = '/api/?r=apis/tareas-flujo/delete'
  urlViewTareasEmpleado = '/api/?r=apis/tareas-flujo/view'

  //------------------------------------------------------

  //OBTENER DEPENDENCIAS EMPRESA
  //Creando dependencia
  urlCreateDependenciaEmpleado = '/api/?r=apis/dependencia/create'
  urlListarDependenciasEmpresa = '/api/?r=apis/dependencia/list&id_empresa='
  urlListarMisDependencias = '/api/?r=apis/dependencia/mis-dependencias&id_empleado_empresa='

  urlCreateTareaEmpleado = '/api/?r=apis/tareas-empleado/create'
  urlEditarTareaEmpleado = '/api/?r=apis/tareas-empleado/update&id='

  // MIS TAREAS
  urlListarMisTareas = '/api/?r=apis/tareas-empleado/view-tareas-empleado&id_empleado_empresa='

  // SUBTAREAS
  urlCrearSubTarea = '/api/?r=apis/subtarea/create'
  urlListarSubTarea = '/api/?r=apis/subtarea/subtareas-tarea-empleado&id_tarea_empleado='
  urlEditarSubTarea = '/api/?r=apis/subtarea/update&id='
  urlEliminarSubTarea = '/api/?r=apis/subtarea/delete&id='
  ulrListarEstadoSubTarea = '/api/?r=apis/estado-subtarea'
  urlViewSubTarea = '?r=apis/subtarea/view&id='

  constructor(private http: HttpClient) {
    console.log('Probando servicio para obtener empleados')
  }

  // PRUEBAAAAA
  private _refresh = new Subject<void>()
  get refresh() {
    return this._refresh
  }

  // Creamos metodos para tareas

  getTareas(id: string | null) {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    return this.http.get(this.urlListarTareas + id, {
      headers: headers
    })
  }

  getSingleTarea(id: string | null): Observable<TareasI> {
    let direccion = this.urlViewTareas + '&id=' + id
    return this.http.get<TareasI>(direccion)
  }

  createTarea(form: TareasI): Observable<TareasI> {
    let direccion = this.urlCreateTareas
    return this.http.post<TareasI>(direccion, form).pipe(
      tap(() => {
        this._refresh.next();
      })
    )
  }

  putTarea(form: TareasI, id: string | null): Observable<TareasI> {
    let direccion = this.urlUpdateTareas + '&id=' + id;
    return this.http.put<TareasI>(direccion, form)

  }

  deleteTarea(id: string | null): Observable<TareasI> {
    let direccion = this.urlDeleteTareas + '&id=' + id;
    let Options = {
      headers: new HttpHeaders({
        'Content-type': 'aplication/json'
      }),
    }
    return this.http.delete<TareasI>(direccion, Options)
  }

  // TERMINO TAREA


  // INICIO ESTADOS TAREAS
  getEstadosTareas() {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    return this.http.get(this.urlListarEstadoTareas, {
      headers: headers
    })
  }

  getSingleEstadosTarea(id: string | null): Observable<EstadosTareasI> {
    let direccion = this.urlViewEstadoTareas + '&id=' + id
    return this.http.get<EstadosTareasI>(direccion)
  }

  createEstadosTarea(form: EstadosTareasI): Observable<EstadosTareasI> {
    let direccion = this.urlCreateEstadoTareas
    return this.http.post<EstadosTareasI>(direccion, form).pipe(
      tap(() => {
        this._refresh.next();
      })
    )
  }

  putEstadosTarea(form: MisTareasI, id: string | null): Observable<MisTareasI> {
    let direccion = this.urlUpdateEstadoTareas + id;
    return this.http.put<MisTareasI>(direccion, form)

  }

  deleteEstadosTarea(id: string | null): Observable<EstadosTareasI> {
    let direccion = this.urlDeleteEstadoTareas + '&id=' + id;
    let Options = {
      headers: new HttpHeaders({
        'Content-type': 'aplication/json'
      }),
    }
    return this.http.delete<EstadosTareasI>(direccion, Options)
  }

  //FIN ESTADOS-TAREAS


  //FLUJOS DE TRABAJO

  getFlujosDeTrabajo(id: any) {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    return this.http.get(this.urlListarFlujosDeTrabajo + id, {
      headers: headers
    })
  }

  getSingleFlujosDeTrabajo(id: string | null): Observable<FlujosDeTrabajoI> {
    let direccion = this.urlViewFlujosDeTrabajo + '&id=' + id
    return this.http.get<FlujosDeTrabajoI>(direccion)
  }

  createFlujosDeTrabajo(form: FlujosDeTrabajoI): Observable<FlujosDeTrabajoI> {
    let direccion = this.urlCreateFlujosDeTrabajo
    return this.http.post<FlujosDeTrabajoI>(direccion, form).pipe(
      tap(() => {
        this._refresh.next();
      })
    )
  }

  putFlujosDeTrabajo(form: FlujosDeTrabajoI, id: string | null): Observable<FlujosDeTrabajoI> {
    let direccion = this.urlUpdateFlujosDeTrabajo + '&id=' + id;
    return this.http.put<FlujosDeTrabajoI>(direccion, form)

  }

  deleteFlujosDeTrabajo(id: string | null): Observable<FlujosDeTrabajoI> {
    let direccion = this.urlDeleteFlujosDeTrabajo + '&id=' + id;
    let Options = {
      headers: new HttpHeaders({
        'Content-type': 'aplication/json'
      }),
    }
    return this.http.delete<FlujosDeTrabajoI>(direccion, Options)
  }

  addTareaFlujo(form: TareaFlujoI): Observable<TareaFlujoI> {
    let direccion = this.urlAddTareaFlujo
    return this.http.post<TareaFlujoI>(direccion, form).pipe(
      tap(() => {
        this._refresh.next();
      })
    )
  }

  getTareasFlujo(id: any) {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    return this.http.get(this.urlListarTareaFlujo + id, {
      headers: headers
    })
  }

  // ASIGNAR TAREA
  asignarFlujoEmpleado(form: FlujoEmpleadoI): Observable<FlujoEmpleadoI> {
    let direccion = this.urlAsignarFlujo
    return this.http.post<FlujoEmpleadoI>(direccion, form).pipe(
      tap(() => {
        this._refresh.next();
      })
    )
  }

  getMisFlujos(id: string | null) {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    return this.http.get(this.urlMisFlujos + id, {
      headers: headers
    })
  }
  getFlujosEmpleado(id: string | null) {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    return this.http.get(this.urlMisFlujos + id, { headers: headers })
  }

  getTareasMiFlujo(idFecha: any, id: any) {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    return this.http.get(this.urlTareasMiFlujo + idFecha + '&id_empleado_responsable='+ id, {
      headers: headers
    })

  }

  putTareaMiFlujo(form: TareasI, id: string | null): Observable<TareasI> {
    let direccion = this.urlUpdateTareaMiFlujo + id;
    return this.http.put<TareasI>(direccion, form)
  }
  //FIN FLUJOS DE TRABAJO

  // OBTENIENDO DASHBOARD CON INFORMACION DE LA EMPRESA

  getdashboard() {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    return this.http.get(this.urldashboard, {
      headers: headers
    })
  }

  


  //INICIO TAREAS FLUJO

  getTareasFlujoDeTrabajo() {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    return this.http.get(this.urlListarTareasFlujosDeTrabajo, {
      headers: headers
    })
  }

  getSingleTareasFlujosDeTrabajo(id: string | null): Observable<TareasFlujosDeTrabajoI> {
    let direccion = this.urlViewTareasFlujosDeTrabajo + '&id=' + id
    return this.http.get<TareasFlujosDeTrabajoI>(direccion)
  }

  createTareasFlujosDeTrabajo(form: TareasFlujosDeTrabajoI): Observable<TareasFlujosDeTrabajoI> {
    let direccion = this.urlCreateTareasFlujosDeTrabajo
    return this.http.post<TareasFlujosDeTrabajoI>(direccion, form).pipe(
      tap(() => {
        this._refresh.next();
      })
    )
  }

  putTareasFlujosDeTrabajo(form: TareasFlujosDeTrabajoI, id: string | null): Observable<TareasFlujosDeTrabajoI> {
    let direccion = this.urlUpdateTareasFlujosDeTrabajo + '&id=' + id;
    return this.http.put<TareasFlujosDeTrabajoI>(direccion, form)

  }

  deleteTareasFlujosDeTrabajo(id: string | null): Observable<TareasFlujosDeTrabajoI> {
    let direccion = this.urlDeleteTareasFlujosDeTrabajo + '&id=' + id;
    let Options = {
      headers: new HttpHeaders({
        'Content-type': 'aplication/json'
      }),
    }
    return this.http.delete<TareasFlujosDeTrabajoI>(direccion, Options)
  }

  //FIN TAREAS FLUJO


  // OBTENER TAREAS
  getTareasEmpleado() {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    return this.http.get(this.urlObtenerTareasEmpleado, { headers: headers })
  }



  // DEPENDENCIA EMPLEADOS
  createDependenciaEmpleado(form: DependenciaEmpleadoI): Observable<DependenciaEmpleadoI> {
    let direccion = this.urlCreateDependenciaEmpleado
    return this.http.post<DependenciaEmpleadoI>(direccion, form).pipe(
      tap(() => {
        this._refresh.next();
      })
    )
  }

  getDependenciaEmpleado(id: string | null) {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    return this.http.get(this.urlListarMisDependencias + id, {
      headers: headers
    })
  }

  // DEPENDENCIA TAREAS
  createDependenciaTareas(form: DependenciaTareaI) {
    let direccion = this.urlCreateTareaEmpleado
    return this.http.post<DependenciaTareaI>(direccion, form).pipe(
      tap(() => {
        this._refresh.next();
      })
    )
  }

  editarDependenciaTareas(form: DependenciaTareaI, id: string | null): Observable<DependenciaTareaI> {
    let direccion = this.urlEditarTareaEmpleado + id
    return this.http.put<DependenciaTareaI>(direccion, form).pipe(
      tap(() => {
        this._refresh.next();
      })
    )
  }

  /*
  getSingleDependenciaTareas(id: string | null): Observable<DependenciaTareaI>{
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')
  
    return this.http.get(this.urlListarMisTareas +id , {
      headers: headers
    })
  }
  */

  getMisTareas(id: string | null) {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    return this.http.get(this.urlListarMisTareas + id, {
      headers: headers
    })
  }



  // CREAR SUBTAREAS
  createSubTarea(form: SubTareaI) {
    let direccion = this.urlCrearSubTarea
    return this.http.post<SubTareaI>(direccion, form).pipe(
      tap(() => {
        this._refresh.next();
      })
    )
  }

  getSubTarea(id: string | null) {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    return this.http.get(this.urlListarSubTarea + id, {
      headers: headers
    })
  }

  getSingleSubTarea(id: string | null) {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    return this.http.get(this.urlViewSubTarea + id, {
      headers: headers
    })
  }

  putSubTarea(form: SubTareaI, id: string | null): Observable<SubTareaI> {
    let direccion = this.urlEditarSubTarea + id;
    return this.http.put<SubTareaI>(direccion, form)

  }

  deleteSubTarea(id: string | null): Observable<SubTareaI> {
    let direccion = this.urlEliminarSubTarea + id;
    let Options = {
      headers: new HttpHeaders({
        'Content-type': 'aplication/json'
      }),
    }
    return this.http.delete<SubTareaI>(direccion, Options)
  }

  getEstadosSubTareas() {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    return this.http.get(this.ulrListarEstadoSubTarea, {
      headers: headers
    })
  }



}
