import { HttpClient, HttpClientModule, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { User, UserResponse } from 'src/app/models/auth/user.interface';

import { Router } from '@angular/router';

//importando herlper
import { JwtHelperService } from '@auth0/angular-jwt'

import { EmpleadosEmpresaI } from 'src/app/models/organizacion/empleadoEmpresa.interface';

// FIN IMPORTACIONES

const helper = new JwtHelperService();

var payload = new FormData();

payload.append('username', 'usuario1')
payload.append('password', 'usuario1')


@Injectable({
  providedIn: 'root'
})

export class AuthService {
  public loggedIn = new BehaviorSubject<boolean>(false);

  url_login = "/api/?r=apis/login/login"



  constructor(private http: HttpClient, private router: Router) { /*this.checkToken()*/ }

  get isLogged(): Observable<boolean> {
    return this.loggedIn.asObservable();
  }


  login(authData: User): Observable<UserResponse> {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('enctype', 'multipart/form-data;')

    return this.http.post<UserResponse>(this.url_login, authData, { headers: headers })
      .pipe(
        map((res: UserResponse) => {
          console.log("Res->", res);
          this.saveToken(res.access_token)
          this.saveUser(res.nombres, res.rol, res.id_empresa, res.nombre_empresa, res.id_empleado_empresa)
          this.loggedIn.next(true);
          return res
          //saveToken()
        }),
        catchError((err) => this.handlerError(err)))
  }

  logout(): void {
    localStorage.removeItem('token')
    sessionStorage.removeItem('nombre_usuario')
    sessionStorage.removeItem('rol_usuario')
    sessionStorage.removeItem('id_empresa')
    sessionStorage.removeItem('nombre_empresa')
    sessionStorage.removeItem('id_empleado_empresa')
    this.loggedIn.next(false);
    //set userIsLogged = false
    this.router.navigate(['/login'])

  }

  checkToken(): void {
    const userToken = localStorage.getItem('token')
    /*const isExpired = helper.isTokenExpired(userToken)
    console.log("isExpired->", isExpired)
    if (isExpired){
      this.logout()
    } else {
      this.loggedIn.next(true)
    }
     //set userIsLogged = isExpired */
  }

  obtenertoken() {
    const userToken = localStorage.getItem('token')
    return userToken
  }


  public saveToken(token: string): void {
    localStorage.setItem('token', token)
  }



  private handlerError(err: { message: any; }): Observable<never> {

    let errorMessage = "Un error ocurrio obteniendo la data";
    if (err) {
      errorMessage = `Error: code ${err.message}`;
    }
    window.alert(errorMessage)
    return throwError(errorMessage)
  }


  //prueba
  // prueba
  public saveUser(user: string, rol:string, id_empresa:string, nombre_empresa:string, id_empleado_empresa:string): void {
    let user_str = user
    let rol_str = rol
    let id_empresa_str = id_empresa
    let nombre_empresa_str = nombre_empresa
    let id_empleado_empresa_str = id_empleado_empresa
    sessionStorage.setItem("nombre_usuario", user_str)
    sessionStorage.setItem("rol_usuario", rol_str)
    sessionStorage.setItem("id_empresa", id_empresa_str)
    sessionStorage.setItem("nombre_empresa", nombre_empresa_str)
    sessionStorage.setItem("mi_id", id_empleado_empresa_str)
  }

  getCurrentUser(){
    let user_str:any = sessionStorage.getItem("nombre_usuario")
    if(user_str != null || user_str !=undefined){
      let user = user_str
      return user;
    } else {
      return null
    }
  }


  //checkear si ugsuario esta logado
  //verificando si usuario esta logado
  onCheckUser():void{
    if (this.getCurrentUser() === null){
      this.loggedIn.next(false);
    } else {
      this.loggedIn.next(true);
    }
  }

  obtenerRolUsuario(rol:any){
    rol = sessionStorage.getItem("rol_usuario")

    return rol
  }

  public funcionario = new BehaviorSubject<boolean>(false);

  get isFuncionario(): Observable<boolean> {
    return this.funcionario.asObservable();
  }

  // Verificar ROles

  public AdminIn = new BehaviorSubject<boolean>(false);
  get isAdmin(): Observable<boolean> {
    return this.AdminIn.asObservable();
    console.log("sin permisos")
  }

}
