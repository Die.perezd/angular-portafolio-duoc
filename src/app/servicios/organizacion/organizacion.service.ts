import { Injectable } from '@angular/core';

//importamos httpClient
import { HttpClient, HttpHeaders } from '@angular/common/http'

// Importando Interface
import { EmpresasI } from './../../models/empresas/empresa.interface'
import { Observable, Subject } from 'rxjs';
import { tap } from 'rxjs/operators';

import { AuthService } from '../auth/auth.service';
import { AreasI } from 'src/app/models/organizacion/areas.interface';
import { RolesI } from 'src/app/models/organizacion/roles.interface';
import { UnidadesI } from 'src/app/models/organizacion/unidades.interface';
import { EmpleadosEmpresaI } from 'src/app/models/organizacion/empleadoEmpresa.interface';
import { DependenciaEmpleadoI } from 'src/app/models/tareas/dependencia.interface';




@Injectable({
  providedIn: 'root'
})
export class OrganizacionService {

  // Creamos una url variable para empresas
  urlListarEmpresas = '/api/?r=apis/empresa'
  urlUpdateEmpresas = '/api/?r=apis/empresa/update'
  urlCreateEmpresas = '/api/?r=apis/empresa/create'
  urlDeleteEmpresas = '/api/?r=apis/empresa/delete'
  urlViewEmpresas = '/api/?r=apis/empresa/view'
  // ----------------------------------------

  // Creamos una url variable para areas
  urlListarAreas = '/api/?r=apis/area/list&id_empresa='
  urlUpdateAreas = '/api/?r=apis/area/update'
  urlCreateAreas = '/api/?r=apis/area/create'
  urlDeleteAreas = '/api/?r=apis/area/delete'
  urlViewAreas = '/api/?r=apis/area/view'
  // ----------------------------------------


  // Creamos una url variable para empleados-empresa
  urlListarEmpleadoEmpresa = '/api/?r=apis/empleado-empresa/list&id_empresa='
  urlUpdateEmpleadoEmpresa = '/api/?r=apis/empleado-empresa/update'
  urlCreateEmpleadoEmpresa = '/api/?r=apis/empleado-empresa/create'
  urlDeleteEmpleadoEmpresa = '/api/?r=apis/empleado-empresa/delete'
  urlViewEmpleadoEmpresa = '/api/?r=apis/empleado-empresa/view'
  // ----------------------------------------

  // Creamos las url para roles
  urlListarRoles = '/api/?r=apis/rol'
  urlUpdateRoles = '/api/?r=apis/rol/update'
  urlCreateRoles = '/api/?r=apis/rol/create'
  urlDeleteRoles = '/api/?r=apis/rol/delete'
  urlViewRoles = '/api/?r=apis/rol/view'

  // ----------------------------------------

  // Creamos las url para unidades
  urlListarUnidades = '/api/?r=apis/unidad/list&id_empresa='
  urlUpdateUnidades = '/api/?r=apis/unidad/update'
  urlCreateUnidades = '/api/?r=apis/unidad/create'
  urlDeleteUnidades = '/api/?r=apis/unidad/delete'
  urlViewUnidades = '/api/?r=apis/unidad/view'

  //Creando dependencia
  urlCreateDependenciaEmpleado = '/api/?r=apis/dependencia/create'
  


  constructor(private http: HttpClient, private auth: AuthService) {
    console.log('Probando servicio para obtener empleados')
  }

  // PRUEBAAAAA
  private _refresh = new Subject<void>()
  get refresh() {
    return this._refresh
  }

  // INICIO EMPRESAS
  getEmpresas() {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    return this.http.get(this.urlListarEmpresas, {
      headers: headers
    })
  }

  getSingleEmpresas(id: string | null): Observable<EmpresasI> {
    let direccion = this.urlViewEmpresas + '&id=' + id
    return this.http.get<EmpresasI>(direccion)
  }

  createEmpresa(form: EmpresasI): Observable<EmpresasI> {
    let direccion = this.urlCreateEmpresas
    return this.http.post<EmpresasI>(direccion, form).pipe(
      tap(() => {
        this._refresh.next();
      })
    )
  }

  putEmpresa(form: EmpresasI, id: string | null): Observable<EmpresasI> {
    let direccion = this.urlUpdateEmpresas + '&id=' + id;
    return this.http.put<EmpresasI>(direccion, form)

  }

  deleteEmpresas(id: string | null): Observable<EmpresasI> {
    let direccion = this.urlDeleteEmpresas + '&id=' + id;
    let Options = {
      headers: new HttpHeaders({
        'Content-type': 'aplication/json'
      }),
    }
    return this.http.delete<EmpresasI>(direccion, Options)
  }


  // FIN EMPRESAS

  // Comienzo areas
  getAreas(id:string|null) {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    //let id = sessionStorage.getItem("id")

    return this.http.get(this.urlListarAreas + id , {
      headers: headers
    })
  }

  getSingleAreas(id: string | null): Observable<AreasI> {
    let direccion = this.urlViewAreas + '&id=' + id
    return this.http.get<AreasI>(direccion)
  }

  createAreas(form: AreasI): Observable<AreasI> {
    let direccion = this.urlCreateAreas
    return this.http.post<AreasI>(direccion, form).pipe(
      tap(() => {
        this._refresh.next();
      })
    )
  }

  putAreas(form: AreasI, id: string | null): Observable<AreasI> {
    let direccion = this.urlUpdateAreas + '&id=' + id;
    return this.http.put<AreasI>(direccion, form)

  }

  deleteAreas(id: string | null): Observable<AreasI> {
    let direccion = this.urlDeleteAreas + '&id=' + id;
    let Options = {
      headers: new HttpHeaders({
        'Content-type': 'aplication/json'
      }),
    }
    return this.http.delete<AreasI>(direccion, Options)
  }

  // fin AREAS

  // Comienzo Colaboradores Empresa

  getEmpleadosEmpresa(id:string|null) {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    return this.http.get(this.urlListarEmpleadoEmpresa + id, {
      headers: headers
    })
  }

  getSingleEmpleadoEmpresas(id: string | null): Observable<EmpleadosEmpresaI> {
    let direccion = this.urlViewEmpleadoEmpresa + '&id=' + id
    return this.http.get<EmpleadosEmpresaI>(direccion)
  }

  createEmpleadoEmpresas(form: EmpleadosEmpresaI): Observable<EmpleadosEmpresaI> {
    let direccion = this.urlCreateEmpleadoEmpresa
    return this.http.post<EmpleadosEmpresaI>(direccion, form).pipe(
      tap(() => {
        this._refresh.next();
      })
    )
  }

  putEmpleadoEmpresas(form: EmpleadosEmpresaI, id: string | null): Observable<EmpleadosEmpresaI> {
    let direccion = this.urlUpdateEmpleadoEmpresa + '&id=' + id;
    return this.http.put<EmpleadosEmpresaI>(direccion, form)

  }

  deleteEmpleadoEmpresas(id: string | null): Observable<EmpleadosEmpresaI> {
    let direccion = this.urlDeleteEmpleadoEmpresa + '&id=' + id;
    let Options = {
      headers: new HttpHeaders({
        'Content-type': 'aplication/json'
      }),
    }
    return this.http.delete<EmpleadosEmpresaI>(direccion, Options)
  }

  // Termino colaboradores Empresa


  // comienzo EmpleadoEmpresas
  getRoles() {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    return this.http.get(this.urlListarRoles, {
      headers: headers
    })
  }

  getSingleRoles(id: string | null): Observable<RolesI> {
    let direccion = this.urlViewRoles + '&id=' + id
    return this.http.get<RolesI>(direccion)
  }

  createRoles(form: RolesI): Observable<RolesI> {
    let direccion = this.urlCreateRoles
    return this.http.post<RolesI>(direccion, form).pipe(
      tap(() => {
        this._refresh.next();
      })
    )
  }

  putRoles(form: RolesI, id: string | null): Observable<RolesI> {
    let direccion = this.urlUpdateRoles + '&id=' + id;
    return this.http.put<RolesI>(direccion, form)

  }

  deleteRoles(id: string | null): Observable<RolesI> {
    let direccion = this.urlDeleteRoles + '&id=' + id;
    let Options = {
      headers: new HttpHeaders({
        'Content-type': 'aplication/json'
      }),
    }
    return this.http.delete<RolesI>(direccion, Options)
  }


  // Termino Roles

  getUnidades(id:string|null) {
    let headers = new HttpHeaders().set('Type-content', 'aplication/json')
      .set('Type-content', 'aplication/json')

    return this.http.get(this.urlListarUnidades +id, {
      headers: headers
    })
  }

  getSingleUnidades(id: string | null): Observable<UnidadesI> {
    let direccion = this.urlViewUnidades + '&id=' + id
    return this.http.get<UnidadesI>(direccion)
  }

  createUnidades(form: UnidadesI): Observable<UnidadesI> {
    let direccion = this.urlCreateUnidades
    return this.http.post<UnidadesI>(direccion, form).pipe(
      tap(() => {
        this._refresh.next();
      })
    )
  }

  putUnidades(form: UnidadesI, id: string | null): Observable<UnidadesI> {
    let direccion = this.urlUpdateUnidades + '&id=' + id;
    return this.http.put<UnidadesI>(direccion, form)

  }

  deleteUnidades(id: string | null): Observable<UnidadesI> {
    let direccion = this.urlDeleteUnidades + '&id=' + id;
    let Options = {
      headers: new HttpHeaders({
        'Content-type': 'aplication/json'
      }),
    }
    return this.http.delete<UnidadesI>(direccion, Options)
  }


  // CREANDO DEPENDENCIA DE EMPLEADOS
  // OBTENIENDO DEPENDENCIA DE TABLA EMPRESA
  createDependenciaEmpleado(form: DependenciaEmpleadoI): Observable<DependenciaEmpleadoI> {
    let direccion = this.urlCreateDependenciaEmpleado
    return this.http.post<DependenciaEmpleadoI>(direccion, form).pipe(
      tap(() => {
        this._refresh.next();
      })
    )
  }

  /*
  deleteDependenciaEmpleado(form: DependenciaEmpleadoI): Observable<DependenciaEmpleadoI> {
    let direccion = this.urlCreateDependenciaEmpleado
    return this.http.delete<DependenciaEmpleadoI>(direccion, form).pipe(
      tap(() => {
        this._refresh.next();
      })
    )
  }
  */

}
