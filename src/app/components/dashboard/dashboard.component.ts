import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { TareasYFlujosService } from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';

/* 
import * as Highcharts from 'highcharts';
// Alternatively, this is how to load Highcharts Stock. The Maps and Gantt
// packages are similar.
// import Highcharts from 'highcharts/highstock';

// Load the exporting module.
import * as Exporting from 'highcharts/modules/exporting';
// Initialize exporting module.
Exporting(Highcharts);
*/


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor( private router: Router, private api: TareasYFlujosService, private fb: FormBuilder,
    public authService: AuthService, public alertas: AlertasService, private sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    this.verificarRolAdmin()
    this.authService.onCheckUser()
  }

  changeUrl() {
    let id_empresa = sessionStorage.getItem("id_empresa")
    let url = 'http://localhost/portafolio/portafolio-duoc-2021-backend/web/index.php?r=apis/dashboard/tareas-por-mes-asignacion&id_empresa='+id_empresa
    
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  // configurando rol y alerta
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  verificarRolAdmin() {
    const rol_usuario = sessionStorage.getItem("rol_usuario")
    if (rol_usuario == "Administrador") {
      this.isAdministrador = true
      this.isFuncionario = true
    } else {
      this.alertas.errorPermisos()
    }
    return rol_usuario
  }

  //FIN configurando ROL


}


