import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { TareasYFlujosService } from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';

@Component({
  selector: 'app-dashboard-tareas',
  templateUrl: './dashboard-tareas.component.html',
  styleUrls: ['./dashboard-tareas.component.css']
})
export class DashboardTareasComponent implements OnInit {

  constructor( private router: Router, private api: TareasYFlujosService, private fb: FormBuilder,
    public authService: AuthService, public alertas: AlertasService, private sanitizer: DomSanitizer) { }

  ngOnInit(): void {

  }

  changeUrl() {
    let id_empresa = sessionStorage.getItem("id_empresa")
    let url = 'http://localhost/portafolio/portafolio-duoc-2021-backend/web/index.php?r=apis/dashboard/tareas-por-mes-asignacion&id_empresa='+id_empresa
    
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  

}
