import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RolesI } from 'src/app/models/organizacion/roles.interface';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-rol',
  templateUrl: './editar-rol.component.html',
  styleUrls: ['./editar-rol.component.css']
})
export class EditarRolComponent implements OnInit {

  constructor(private activerouter: ActivatedRoute, private router:Router, private api:OrganizacionService, private fb: FormBuilder,
    public authService:AuthService) { }

  datosRoles!: RolesI;
  editarForm = this.fb.group({
    id_rol: new FormControl(),
    nombre_rol: new FormControl(''),
    descripcion_rol: new FormControl(''),
  })

  ngOnInit(): void {
    this.obtenerDataRoles()
    this.verificarRolAdmin()
    this.authService.onCheckUser()
  }

  postForm(form: RolesI) {
    let rolid = this.activerouter.snapshot.paramMap.get('id')

    Swal.fire({
      title: '¿Quieres guardar tus cambios??',
      showDenyButton: true,
      confirmButtonText: 'Confirmar',
      denyButtonText: `Cancelar`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        Swal.fire('Cambios realizados!', '', 'success')
        this.api.putRoles(form, rolid).subscribe(data => {
          let respuesta: RolesI = data
          this.router.navigate(['/listarRoles'])
          
        })
      } else if (result.isDenied) {
        Swal.fire('Los cambios no fueron guardados', '', 'info')
        this.router.navigate(['/listarRoles'])
      }
    })

  }

  obtenerDataRoles(){
    let rolid = this.activerouter.snapshot.paramMap.get('id')

    this.api.getSingleRoles(rolid).subscribe((data:any) => {
      
      this.datosRoles = data
      this.editarForm.patchValue({
        "id_rol": this.datosRoles.id_rol,
        "nombre_rol": this.datosRoles.nombre_rol,
        "descripcion_rol": this.datosRoles.descripcion_rol,
      })

    })
  }

  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  obtenerRol(){
    const rol_usuario = sessionStorage.getItem("rol_usuario")

    return rol_usuario
  }

  verificarRolAdmin(){
    if(this.obtenerRol() == "Administrador"){
      this.isAdministrador = true
      this.isFuncionario = true
    } else {
      Swal.fire(
        'Error',
        'No cuenta con los permisos para acceder a esta funcionalidad',
        'error'
      ).then((result) => {
        this.router.navigate(['/principal'])
    });
    }
    return this.obtenerRol()
  }

  //FIN configurando ROL

}
