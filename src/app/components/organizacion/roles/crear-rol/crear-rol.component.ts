import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { RolesI } from 'src/app/models/organizacion/roles.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service';

@Component({
  selector: 'app-crear-rol',
  templateUrl: './crear-rol.component.html',
  styleUrls: ['./crear-rol.component.css']
})
export class CrearRolComponent implements OnInit {

   //this.authService.onCheckUser()

   datosRoles!: RolesI;
   nuevoForm = this.fb.group({
     id_rol: new FormControl(),
     nombre_rol: new FormControl(''),
     descripcion_rol: new FormControl('')
   })
 
   constructor(private fb: FormBuilder, private activerouter: ActivatedRoute, private router: Router, private api: OrganizacionService,
     private dialogRef: MatDialog, public authService: AuthService, private alertas: AlertasService) { }
 
     ngOnInit(): void {
       this.verificarRolAdmin()
       this.authService.onCheckUser()
     }
   
     postForm(form: RolesI) {
       if (this.nuevoForm.valid) {
         this.api.createRoles(form).subscribe(data => {
           this.alertas.ShowSucces2()
           this.router.navigate(["/listarRoles"])
         }, err => {
           let message = "<ul>"
           err.error.forEach((element: { field: string; message: string; }) => {
             message += "<li> " + element.message + "</li> <hr>"
           });
           message += "</ul>"
           this.alertas.showFormError(message)
           console.log("Este es una captura de error: ", err.error)
         })
   
       }
     }
 
 
   // Obteniendo data del form
   get nombre_rol() { return this.nuevoForm.get('nombre_rol'); }
   get descripcion_rol() { return this.nuevoForm.get('descripcion_rol'); }
 
   
   // configurando rol y alerta 
   isAdministrador = false
   isFuncionario = false
   isDisenador = false
 
   verificarRolAdmin(){
     const rol_usuario = sessionStorage.getItem("rol_usuario")
     if(rol_usuario == "Administrador"){
       this.isAdministrador = true
       this.isFuncionario = true
     } else {
       this.alertas.errorPermisos()
     }
     return rol_usuario
   }
 
   //FIN configurando ROL

}
