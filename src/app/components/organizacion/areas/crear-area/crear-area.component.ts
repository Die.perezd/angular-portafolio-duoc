/* eslint-disable eqeqeq */
import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormControl } from '@angular/forms'
import { MatDialog } from '@angular/material/dialog'
import { Router } from '@angular/router'
import { AreasI } from 'src/app/models/organizacion/areas.interface'
import { AlertasService } from 'src/app/servicios/alertas/alertas.service'
import { AuthService } from 'src/app/servicios/auth/auth.service'
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service'

@Component({
  selector: 'app-crear-area',
  templateUrl: './crear-area.component.html',
  styleUrls: ['./crear-area.component.css']
})
export class CrearAreaComponent implements OnInit {
  datosAreas!: AreasI;
  nuevoForm = this.fb.group({
    nombre_area: new FormControl(),
    descripcion_area: new FormControl(''),
    id_empresa: new FormControl(sessionStorage.getItem('id_empresa'))
  })

  // eslint-disable-next-line no-useless-constructor
  constructor (private fb:FormBuilder, private router: Router, private orgSvc: OrganizacionService,
    private dialogRef: MatDialog, public authService: AuthService, private alertas: AlertasService) { }

  ngOnInit (): void {
    this.verificarRolAdmin()
    this.authService.onCheckUser()
  }

  closeDialog () {
    this.dialogRef.closeAll()
  }

  postForm (form: AreasI) {
    if (this.nuevoForm.valid) {
      this.orgSvc.createAreas(form).subscribe(data => {
        this.alertas.ShowSucces2()
        this.router.navigate(['/listarAreas'])
      }, err => {
        let message = '<ul>'
        err.error.forEach((element: { field: string; message: string; }) => {
          message += '<li> ' + element.message + '</li> <hr>'
        })
        message += '</ul>'
        this.alertas.showFormError(message)
        console.log('Este es una captura de error: ', err.error)
      })
    } else {
      const message = '<h2> Los campos no pueden ser vacíos </h2>'
      this.alertas.showFormError(message)
      console.log('Not Valid')
    }
  }

  salir () {
    this.dialogRef.closeAll()
    this.router.navigate(['listarColaboradores'])
  }

  // Obteniendo data del form
  // eslint-disable-next-line camelcase
  get nombre_area () { return this.nuevoForm.get('nombre_area') }
  // eslint-disable-next-line camelcase
  get descripcion_area () { return this.nuevoForm.get('descripcion_area') }

  // configurando rol y alerta
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  verificarRolAdmin () {
    // eslint-disable-next-line camelcase
    const rol_usuario = sessionStorage.getItem('rol_usuario')
    // eslint-disable-next-line camelcase
    // eslint-disable-next-line eqeqeq
    // eslint-disable-next-line camelcase
    if (rol_usuario == 'Administrador') {
      this.isAdministrador = true
      this.isFuncionario = true
    } else {
      this.alertas.errorPermisos()
    }
    // eslint-disable-next-line camelcase
    return rol_usuario
  }

  // FIN configurando ROL
}
