/* eslint-disable eqeqeq */
import { Component, OnInit } from '@angular/core'
import { FormBuilder, FormControl } from '@angular/forms'
import { ActivatedRoute, Router } from '@angular/router'
import { AreasI } from 'src/app/models/organizacion/areas.interface'
import { AuthService } from 'src/app/servicios/auth/auth.service'
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-editar-area',
  templateUrl: './editar-area.component.html',
  styleUrls: ['./editar-area.component.css']
})
export class EditarAreaComponent implements OnInit {
  // eslint-disable-next-line no-useless-constructor
  constructor (private activerouter: ActivatedRoute, private router:Router, private api:OrganizacionService, private fb: FormBuilder,
   public authService:AuthService) { }

  datosAreas!: AreasI;
  editarForm = this.fb.group({
    id_area: new FormControl(),
    nombre_area: new FormControl(''),
    descripcion_area: new FormControl(''),
    id_empresa: new FormControl(sessionStorage.getItem('id_empresa'))
  })

  ngOnInit (): void {
    this.obtenerDataAreas()
    this.verificarRolAdmin()
    this.authService.onCheckUser()
  }

  postForm (form: AreasI) {
    const areaid = this.activerouter.snapshot.paramMap.get('id')

    Swal.fire({
      title: '¿Quieres guardar tus cambios??',
      showDenyButton: true,
      confirmButtonText: 'Confirmar',
      denyButtonText: 'Cancelar'
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        Swal.fire('Cambios realizados!', '', 'success')
        this.api.putAreas(form, areaid).subscribe(data => {
          this.router.navigate(['/listarAreas'])
        })
      } else if (result.isDenied) {
        Swal.fire('Los cambios no fueron guardados', '', 'info')
        this.router.navigate(['/listarAreas'])
      }
    })
  }

  obtenerDataAreas () {
    const areaid = this.activerouter.snapshot.paramMap.get('id')

    this.api.getSingleAreas(areaid).subscribe((data:any) => {
      this.datosAreas = data
      this.editarForm.patchValue({
        id_area: this.datosAreas.id_area,
        nombre_area: this.datosAreas.nombre_area,
        descripcion_area: this.datosAreas.descripcion_area
      })
    })
  }

  // configurando rol y alerta
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  obtenerRol () {
    // eslint-disable-next-line camelcase
    const rol_usuario = sessionStorage.getItem('rol_usuario')

    // eslint-disable-next-line camelcase
    return rol_usuario
  }

  verificarRolAdmin () {
    if (this.obtenerRol() == 'Administrador') {
      this.isAdministrador = true
      this.isFuncionario = true
    } else {
      Swal.fire(
        'Error',
        'No cuenta con los permisos para acceder a esta funcionalidad',
        'error'
      ).then((result) => {
        this.router.navigate(['/principal'])
      })
    }
    return this.obtenerRol()
  }
}
