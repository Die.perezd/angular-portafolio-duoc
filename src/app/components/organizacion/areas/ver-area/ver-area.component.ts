import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AreasI } from 'src/app/models/organizacion/areas.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ver-area',
  templateUrl: './ver-area.component.html',
  styleUrls: ['./ver-area.component.css']
})
export class VerAreaComponent implements OnInit {
  // eslint-disable-next-line no-useless-constructor
  constructor(private activerouter: ActivatedRoute, private router:Router, private api:OrganizacionService, private fb: FormBuilder,
    public authService:AuthService, @Inject(MAT_DIALOG_DATA) public data: any, private dialogRef:MatDialog) { }

  datosAreas!: AreasI;
  editarForm = this.fb.group({
    id_area: new FormControl(),
    nombre_area: new FormControl(''),
    descripcion_area: new FormControl(''),
    id_empresa: new FormControl(sessionStorage.getItem('id_empresa'))
  })

  ngOnInit (): void {
    this.obtenerDataAreas()
    this.verificarRolAdmin()
    this.authService.onCheckUser()
    console.log(this.data)
  }

  postForm (form: AreasI) {
    const areaid = this.activerouter.snapshot.paramMap.get('id')

    Swal.fire({
      title: '¿Quieres guardar tus cambios??',
      showDenyButton: true,
      confirmButtonText: 'Confirmar',
      // eslint-disable-next-line quotes
      denyButtonText: `Cancelar`
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        Swal.fire('Cambios realizados!', '', 'success')
        this.api.putAreas(form, areaid).subscribe(data => {
          this.router.navigate(['/listarAreas'])
        })
      } else if (result.isDenied) {
        Swal.fire('Los cambios no fueron guardados', '', 'info')
        this.router.navigate(['/listarAreas'])
      }
    })

  }

  obtenerDataAreas(){
    let areaid = this.activerouter.snapshot.paramMap.get('id')
    let id = this.data

    this.api.getSingleAreas(id).subscribe((data:any) => {
      
      this.datosAreas = data
      this.editarForm.patchValue({
        "id_area": this.datosAreas.id_area,
        "nombre_area": this.datosAreas.nombre_area,
        "descripcion_area": this.datosAreas.descripcion_area,
      })

    })
  }

  closeDialog() {
    this.dialogRef.closeAll()
    this.router.navigate(['listarAreas'])
  }

  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  obtenerRol(){
    const rol_usuario = sessionStorage.getItem("rol_usuario")

    return rol_usuario
  }

  verificarRolAdmin(){
    if(this.obtenerRol() == "Administrador"){
      this.isAdministrador = true
      this.isFuncionario = true
    } else {
      Swal.fire(
        'Error',
        'No cuenta con los permisos para acceder a esta funcionalidad',
        'error'
      ).then((result) => {
        this.router.navigate(['/principal'])
    });
    }
    return this.obtenerRol()
  }

}
