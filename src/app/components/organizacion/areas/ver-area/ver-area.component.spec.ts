import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerAreaComponent } from './ver-area.component';

describe('VerAreaComponent', () => {
  let component: VerAreaComponent;
  let fixture: ComponentFixture<VerAreaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerAreaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerAreaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
