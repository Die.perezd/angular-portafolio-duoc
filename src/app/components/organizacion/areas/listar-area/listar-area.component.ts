/* eslint-disable camelcase */
import { Component, OnInit, ViewChild } from '@angular/core'
import { MatDialog } from '@angular/material/dialog'
import { MatPaginator } from '@angular/material/paginator'
import { MatSort } from '@angular/material/sort'
import { MatTableDataSource } from '@angular/material/table'
import { Router } from '@angular/router'
import { Subscription } from 'rxjs'
import { AreasI } from 'src/app/models/organizacion/areas.interface'
import { AlertasService } from 'src/app/servicios/alertas/alertas.service'
import { AuthService } from 'src/app/servicios/auth/auth.service'
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service'
import Swal from 'sweetalert2'
import { VerAreaComponent } from '../ver-area/ver-area.component'

@Component({
  selector: 'app-listar-area',
  templateUrl: './listar-area.component.html',
  styleUrls: ['./listar-area.component.css']
})
export class ListarAreaComponent implements OnInit {
  public areas: Array<any> = []
  // NUEVO DATA TABLE

  // prueba new data table
  displayedColumns: string[] = ['id_area', 'nombre_area', 'descripcion_area', 'acciones'];

  dataSource!: MatTableDataSource<any>

  applyFilter (event: Event) {
    const filterValue = (event.target as HTMLInputElement).value
    this.dataSource.filter = filterValue.trim().toLowerCase()
  }

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  public listEmpresas: Array<AreasI> = [];

  // eslint-disable-next-line no-useless-constructor
  constructor (private orgSvc:OrganizacionService, private router: Router, private dialogRef: MatDialog,
    public authService: AuthService, public alertas:AlertasService) { }

  // ORUEBA22
  sucription: Subscription | undefined;

  ngOnInit (): void {
    this.cargarAreas()
    this.verificarRolAdmin()

    this.authService.onCheckUser()

    this.sucription = this.orgSvc.refresh.subscribe(() => {
      this.cargarAreas()
    })
  }

  cargarAreas () {
    const id = sessionStorage.getItem('id_empresa')
    this.orgSvc.getAreas(id).subscribe((resp: any) => {
      // console.log(resp)
      this.listEmpresas = resp
      this.dataSource = new MatTableDataSource(this.listEmpresas)
      this.dataSource.paginator = this.paginator
      this.dataSource.sort = this.sort
    })
  }

  ngAfterViewInit () {
    this.dataSource.paginator = this.paginator
    this.dataSource.sort = this.sort
  }

  eliminarArea (id:any) {
    Swal.fire({
      title: '¿Estas seguro?',
      text: 'Esto eliminará el registro',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.orgSvc.deleteAreas(id).subscribe(data => {
          if (result.isConfirmed) {
            Swal.fire(
              'Eliminado!',
              'Registro correctamente eliminado',
              'success'
            )
          }
          this.cargarAreas()
        })
      }
    })
  }

  editarArea (id:any) {
    this.router.navigate(['editarAreas/' + id])
  }

  /*
  verArea(id:any){
    this.router.navigate(['verArea/' + id])
  }
  */

  verArea (id:number) {
    this.dialogRef.open(VerAreaComponent, {
      data: id
    })
  }

  // configurando rol y alerta
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  verificarRolAdmin () {
    // eslint-disable-next-line camelcase
    const rol_usuario = sessionStorage.getItem('rol_usuario')
    // eslint-disable-next-line camelcase
    // eslint-disable-next-line eqeqeq
    if (rol_usuario == 'Administrador') {
      this.isAdministrador = true
      this.isFuncionario = true
    } else {
      this.alertas.errorPermisos()
    }
    return rol_usuario
  }

  // FIN configurando ROL
}
