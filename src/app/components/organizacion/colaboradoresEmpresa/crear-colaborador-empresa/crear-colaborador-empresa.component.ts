import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {ActivatedRoute, Router} from '@angular/router';
import {EmpleadoJefeI, EmpleadosEmpresaI} from 'src/app/models/organizacion/empleadoEmpresa.interface';
import {AlertasService} from 'src/app/servicios/alertas/alertas.service';
import {AuthService} from 'src/app/servicios/auth/auth.service';
import {OrganizacionService} from 'src/app/servicios/organizacion/organizacion.service';
import {TareasYFlujosService} from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';

@Component({
  selector: 'app-crear-colaborador-empresa',
  templateUrl: './crear-colaborador-empresa.component.html',
  styleUrls: ['./crear-colaborador-empresa.component.css']
})
export class CrearColaboradorEmpresaComponent implements OnInit {

  datosEmpleadoEmpresa!: EmpleadosEmpresaI;
  nuevoForm = this.fb.group({
    nombres: new FormControl(''),
    primer_apellido: new FormControl(''),
    segundo_apellido: new FormControl(''),
    numero_documento: new FormControl(''),
    fecha_nacimiento: new FormControl(''),
    direccion: new FormControl(''),
    email: new FormControl(''),
    username: new FormControl(''),
    pass: new FormControl(''),
    id_unidad: new FormControl(''),
    id_area: new FormControl(''),
    id_empresa: new FormControl(sessionStorage.getItem("id_empresa")),
    id_rol: new FormControl(),
    id_empleado_jefe: new FormControl(''),
  })

  dependenciaForm = this.fb.group({
    id_empleado_jefe: new FormControl(''),
    id_empleado_subordinado: new FormControl(''),
  })


  public data: Array<any> = []
  public areas: Array<any> = []
  public roles: Array<any> = []
  public unidades: Array<any> = []
  public dependenciaEmpleado: Array<any> = []

  constructor(private fb: FormBuilder, private activerouter: ActivatedRoute, private router: Router, private orgSvc: OrganizacionService,
              private dialogRef: MatDialog, public authService: AuthService, private alertas: AlertasService, public tyf: TareasYFlujosService) {
  }

  //hide para password
  hide = true;

  // NUEVO INTENTO DE CREAR EMPLEADO - EMPRESA

  ngOnInit(): void {
    this.obtenerData()
    this.obtenerRoles()
    this.obtenerUnidades()
    this.obtenerDataEmpleadosEmpresa()
    //console.log( "CONSULTA EMPLEADOS",this.obtenerEmpleados())
    this.verificarRolAdmin()
    this.authService.onCheckUser()
  }

  postForm(form: EmpleadoJefeI) {
    if (this.nuevoForm.valid) {
      this.orgSvc.createEmpleadoEmpresas(form).subscribe(data => {
        this.alertas.ShowSucces2()
        this.router.navigate(['/listarColaboradoresEmpresa'])
      }, err => {
        let message = "<ul>"
        err.error.forEach((element: { field: string; message: string; }) => {
          message += "<li> " + element.message + "</li> <hr>"
        });
        message += "</ul>"
        this.alertas.showFormError(message)
        console.log("Este es una captura de error: ", err.error)
      })


    }
  }

  obtenerDataEmpleadosEmpresa() {
    let id = sessionStorage.getItem("id_empresa")
    this.orgSvc.getEmpleadosEmpresa(id).subscribe((resp: any) => {
      /* filtrando a los empleado distintos a mi id
      let miID= sessionStorage.getItem("mi_id")
      const filtered = resp.filter(function(element: { id_empleado_empresa: string; }){
        return element.id_empleado_empresa != miID;
      });
      console.log("RESPUESTAAAAA",filtered)
      */

      this.data = resp
    })

  }

  obtenerData() {
    let id = sessionStorage.getItem("id_empresa")
    this.orgSvc.getAreas(id).subscribe((resp: any) => {
      this.areas = resp
      console.log(this.areas)
    })
  }

  obtenerUnidades() {
    let id = sessionStorage.getItem("id_empresa")
    this.orgSvc.getUnidades(id).subscribe((resp: any) => {
      this.unidades = resp
      console.log(this.unidades)
    })
  }

  obtenerRoles() {
    this.orgSvc.getRoles().subscribe((resp: any) => {
      this.roles = resp
      console.log(this.roles)
    })
  }

  get id_empleado() {
    return this.nuevoForm.get('id_empleado');
  }

  get id_empleado_empresa() {
    return this.nuevoForm.get('id_empleado_empresa');
  }

  get id_area() {
    return this.nuevoForm.get('area');
  }

  get id_rol() {
    return this.nuevoForm.get('id_rol');
  }

  get id_unidad() {
    return this.nuevoForm.get('unidad');
  }

  get id_empresa() {
    return this.nuevoForm.get('empresa');
  }

  get username() {
    return this.nuevoForm.get('username');
  }

  get pass() {
    return this.nuevoForm.get('pass');
  }

  // oobteniendo datos para los matError
  get nombres() {
    return this.nuevoForm.get('nombres');
  }

  get primer_apellido() {
    return this.nuevoForm.get('primer_apellido');
  }

  get segundo_apellido() {
    return this.nuevoForm.get('segundo_apellido');
  }

  get numero_documento() {
    return this.nuevoForm.get('numero_documento')
  }

  get email() {
    return this.nuevoForm.get('email')
  }


  //DEPENDENCIA
  get id_empleado_jefe() {
    return this.dependenciaForm.get('id_empleado_jefe');
  }

  get id_empleado_subordinado() {
    return this.dependenciaForm.get('id_empleado_subordinado');
  }

  // configurando rol y alerta
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  verificarRolAdmin() {
    const rol_usuario = sessionStorage.getItem("rol_usuario")
    if (rol_usuario == "Administrador") {
      this.isAdministrador = true
      this.isFuncionario = true
    } else {
      this.alertas.errorPermisos()
    }
    return rol_usuario
  }

  //FIN configurando ROL

}
