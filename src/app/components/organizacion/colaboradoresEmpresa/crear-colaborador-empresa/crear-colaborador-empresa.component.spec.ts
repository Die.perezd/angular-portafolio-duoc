import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearColaboradorEmpresaComponent } from './crear-colaborador-empresa.component';

describe('CrearColaboradorEmpresaComponent', () => {
  let component: CrearColaboradorEmpresaComponent;
  let fixture: ComponentFixture<CrearColaboradorEmpresaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrearColaboradorEmpresaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearColaboradorEmpresaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
