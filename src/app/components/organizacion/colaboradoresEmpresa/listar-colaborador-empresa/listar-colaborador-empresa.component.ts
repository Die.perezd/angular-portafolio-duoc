import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { EmpleadosEmpresaI } from 'src/app/models/organizacion/empleadoEmpresa.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listar-colaborador-empresa',
  templateUrl: './listar-colaborador-empresa.component.html',
  styleUrls: ['./listar-colaborador-empresa.component.css']
})
export class ListarColaboradorEmpresaComponent implements OnInit {

  public colaboradoresEmpresa:Array<EmpleadosEmpresaI> = []
  // NUEVO DATA TABLE

 // prueba new data table
 displayedColumns: string[] = ['id_empleado_empresa', 'nombres', 'apellidos', 'numero_documento',
 'direccion', 'email','id_unidad', 'id_area', 'id_rol', 'acciones'];

 dataSource!: MatTableDataSource<EmpleadosEmpresaI>

 applyFilter(event: Event) {
   const filterValue = (event.target as HTMLInputElement).value;
   this.dataSource.filter = filterValue.trim().toLowerCase();
 }

 @ViewChild(MatPaginator) paginator!: MatPaginator;
 @ViewChild(MatSort) sort!: MatSort;

 public listEmpleados: Array<EmpleadosEmpresaI> = [];

 constructor(private orgSvc:OrganizacionService, private router: Router, private dialogRef: MatDialog,
   public authService: AuthService, public alertas:AlertasService) { }

   // ORUEBA22
 sucription: Subscription | undefined;

 ngOnInit(): void {
   this.cargarEmpleados()
   this.verificarRolAdmin()
   
   this.authService.onCheckUser()

   this.sucription = this.orgSvc.refresh.subscribe(()=> {
     this.cargarEmpleados()
   })

 }

 cargarEmpleados() {
   let id = sessionStorage.getItem("id_empresa")
   this.orgSvc.getEmpleadosEmpresa(id).subscribe((resp: any) => {
     // console.log(resp)
     this.listEmpleados = resp
     this.dataSource = new MatTableDataSource(this.listEmpleados)
     this.dataSource.paginator=this.paginator;
     this.dataSource.sort = this.sort
   })
 }

 ngAfterViewInit() {
   this.dataSource.paginator = this.paginator;
   this.dataSource.sort = this.sort;
 }

 eliminarColaboradoresEmpresa(id:any) {
   Swal.fire({
     title: '¿Estas seguro?',
     text: "Esto eliminará el registro",
     icon: 'warning',
     showCancelButton: true,
     confirmButtonColor: '#3085d6',
     cancelButtonColor: '#d33',
     confirmButtonText: 'Si, eliminar!'
   }).then((result) => {
     if (result.isConfirmed) {
       this.orgSvc.deleteEmpleadoEmpresas(id).subscribe(data => {
         if (result.isConfirmed) {
           Swal.fire(
             'Eliminado!',
             'Registro correctamente eliminado',
             'success'
           )
         }
         this.cargarEmpleados()
       }, err => {
        if(err = 23000){
          Swal.fire(
            'Error',
            'El empleado no puede ser eliminado porque esta asignado a un equipo de trabajo',
            'error'
          )
        }
      })
     }

   })
 }

 editarColaboradoresEmpresa(id:any){
   this.router.navigate(['editarColaboradoresEmpresa/' + id])
 }

 // configurando rol y alerta 
 isAdministrador = false
 isFuncionario = false
 isDisenador = false

 verificarRolAdmin(){
   const rol_usuario = sessionStorage.getItem("rol_usuario")
   if(rol_usuario == "Administrador"){
     this.isAdministrador = true
     this.isFuncionario = true
   } else {
     this.alertas.errorPermisos()
   }
   return rol_usuario
 }

 //FIN configurando ROL

 getNameEmpresa(){
   return sessionStorage.getItem("nombre_empresa")
 }

}
