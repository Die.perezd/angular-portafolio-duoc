import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarColaboradorEmpresaComponent } from './listar-colaborador-empresa.component';

describe('ListarColaboradorEmpresaComponent', () => {
  let component: ListarColaboradorEmpresaComponent;
  let fixture: ComponentFixture<ListarColaboradorEmpresaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarColaboradorEmpresaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarColaboradorEmpresaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
