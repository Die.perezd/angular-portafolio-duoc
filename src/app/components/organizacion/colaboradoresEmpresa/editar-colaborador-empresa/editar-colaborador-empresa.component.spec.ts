import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarColaboradorEmpresaComponent } from './editar-colaborador-empresa.component';

describe('EditarColaboradorEmpresaComponent', () => {
  let component: EditarColaboradorEmpresaComponent;
  let fixture: ComponentFixture<EditarColaboradorEmpresaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarColaboradorEmpresaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarColaboradorEmpresaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
