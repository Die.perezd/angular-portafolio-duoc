import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EmpleadosEmpresaI } from 'src/app/models/organizacion/empleadoEmpresa.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service';
import { PersonasService } from 'src/app/servicios/personas/personas.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-colaborador-empresa',
  templateUrl: './editar-colaborador-empresa.component.html',
  styleUrls: ['./editar-colaborador-empresa.component.css']
})
export class EditarColaboradorEmpresaComponent implements OnInit {

  public data: Array<any> = []
  public roles: Array<any> = []
  public unidades: Array<any> = []
  public empleados: Array<any> = []
  public empresas: Array<any> = []

  constructor(private fb: FormBuilder, private activerouter: ActivatedRoute, private router: Router, private api: OrganizacionService,
    public authService: AuthService, private alertas: AlertasService) { }

  datosEmpleadosEmpresa!: EmpleadosEmpresaI;
  editarForm = this.fb.group({
    id_empleado_empresa: new FormControl(''),
    nombres: new FormControl(''),
    primer_apellido: new FormControl(''),
    segundo_apellido: new FormControl(),
    numero_documento: new FormControl(''),
    fecha_nacimiento: new FormControl(''),
    direccion: new FormControl(''),
    email: new FormControl(''),
    username: new FormControl(''),
    pass: new FormControl(''),
    id_unidad: new FormControl(''),
    id_area: new FormControl(''),
    id_rol: new FormControl(''),
  })

  //hide para password
  hide = true;

  // NUEVO INTENTO DE CREAR EMPLEADO - EMPRESA

  ngOnInit(): void {
    this.obtenerData()
    this.obtenerRoles()
    this.obtenerUnidades()
    this.verificarRolAdmin()
    this.authService.onCheckUser()
    this.obtenerDataColaboradoresEmpresa()

    
  }

  postForm(form: EmpleadosEmpresaI) {
    let empEmpresaid = this.activerouter.snapshot.paramMap.get('id')

    Swal.fire({
      title: '¿Quieres guardar tus cambios??',
      showDenyButton: true,
      confirmButtonText: 'Confirmar',
      denyButtonText: `Cancelar`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        Swal.fire('Cambios realizados!', '', 'success')
        this.api.putEmpleadoEmpresas(form, empEmpresaid).subscribe(data => {
          let respuesta: EmpleadosEmpresaI = data
          this.router.navigate(['/listarColaboradoresEmpresa'])
        })
      } else if (result.isDenied) {
        Swal.fire('Los cambios no fueron guardados', '', 'info')
        this.router.navigate(['/listarColaboradoresEmpresa'])
      }
    })

  }

  obtenerDataColaboradoresEmpresa(){
    let empEmpresaid = this.activerouter.snapshot.paramMap.get('id')

    this.api.getSingleEmpleadoEmpresas(empEmpresaid).subscribe((data:any) => {
      
      this.datosEmpleadosEmpresa = data
      this.editarForm.patchValue({
        "id_empleado_empresa": this.datosEmpleadosEmpresa.id_empleado_empresa,
        "nombres": this.datosEmpleadosEmpresa.nombres,
        "primer_apellido": this.datosEmpleadosEmpresa.primer_apellido,
        "segundo_apellido": this.datosEmpleadosEmpresa.segundo_apellido,
        "numero_documento": this.datosEmpleadosEmpresa.numero_documento,
        "fecha_nacimiento": this.datosEmpleadosEmpresa.fecha_nacimiento,
        "direccion": this.datosEmpleadosEmpresa.direccion,
        "email": this.datosEmpleadosEmpresa.email,
        "username": this.datosEmpleadosEmpresa.username,
        "pass": this.datosEmpleadosEmpresa.pass,
        "id_unidad": this.datosEmpleadosEmpresa.id_unidad,
        "id_area": this.datosEmpleadosEmpresa.id_area,
        "id_rol": this.datosEmpleadosEmpresa.id_rol,
      })

    })
  }

  obtenerData() {
    let id= sessionStorage.getItem("id_empresa")
    this.api.getAreas(id).subscribe((resp: any) => {
      this.data = resp
      console.log(this.data)
    })
  }


  obtenerUnidades() {
    let id = sessionStorage.getItem("id_empresa")
    this.api.getUnidades(id).subscribe((resp: any) => {
      this.unidades = resp
      console.log(this.unidades)
    })
  }

  obtenerRoles() {
    this.api.getRoles().subscribe((resp: any) => {
      this.roles = resp
      console.log(this.roles)
    })
  }

  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  verificarRolAdmin() {
    const rol_usuario = sessionStorage.getItem("rol_usuario")
    if (rol_usuario == "Administrador") {
      this.isAdministrador = true
      this.isFuncionario = true
    } else {
      this.alertas.errorPermisos()
    }
    return rol_usuario
  }

  //FIN configurando ROL

}
