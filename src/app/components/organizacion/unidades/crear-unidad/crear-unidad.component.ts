import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { UnidadesI } from 'src/app/models/organizacion/unidades.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service';

@Component({
  selector: 'app-crear-unidad',
  templateUrl: './crear-unidad.component.html',
  styleUrls: ['./crear-unidad.component.css']
})
export class CrearUnidadComponent implements OnInit {

   //this.authService.onCheckUser()

   datosUnidades!: UnidadesI;
   nuevoForm = this.fb.group({
     id_unidad: new FormControl(),
     nombre_unidad: new FormControl(''),
     descripcion_unidad: new FormControl(''),
     id_empresa: new FormControl(sessionStorage.getItem("id_empresa"))
   })
 
   constructor(private fb: FormBuilder, private activerouter: ActivatedRoute, private router: Router, private api: OrganizacionService,
     private dialogRef: MatDialog, public authService: AuthService, private alertas: AlertasService) { }
 
     ngOnInit(): void {
       this.verificarRolAdmin()
       this.authService.onCheckUser()
     }
   
     postForm(form: UnidadesI) {
       if (this.nuevoForm.valid) {
         this.api.createUnidades(form).subscribe(data => {
           this.alertas.ShowSucces2()
           this.router.navigate(["/listarUnidades"])
         }, err => {
           let message = "<ul>"
           err.error.forEach((element: { field: string; message: string; }) => {
             message += "<li> " + element.message + "</li> <hr>"
           });
           message += "</ul>"
           this.alertas.showFormError(message)
           console.log("Este es una captura de error: ", err.error)
         })
   
       }
     }
 
   // Obteniendo data del form
   get nombre_unidad() { return this.nuevoForm.get('nombre_unidad'); }
   get descripcion_unidad() { return this.nuevoForm.get('descripcion_unidad'); }
 
   
   // configurando rol y alerta 
   isAdministrador = false
   isFuncionario = false
   isDisenador = false
 
   verificarRolAdmin() {
     const rol_usuario = sessionStorage.getItem("rol_usuario")
     if (rol_usuario == "Administrador") {
       this.isAdministrador = true
       this.isFuncionario = true
     } else {
       this.alertas.errorPermisos()
     }
     return rol_usuario
   }
 
   //FIN configurando ROL
 
}
