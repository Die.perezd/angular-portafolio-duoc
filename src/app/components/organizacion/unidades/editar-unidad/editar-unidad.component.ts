import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UnidadesI } from 'src/app/models/organizacion/unidades.interface';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-unidad',
  templateUrl: './editar-unidad.component.html',
  styleUrls: ['./editar-unidad.component.css']
})
export class EditarUnidadComponent implements OnInit {

  constructor(private activerouter: ActivatedRoute, private router:Router, private api:OrganizacionService, private fb: FormBuilder,
    public authService:AuthService) { }

  datosUnidades!: UnidadesI;
  editarForm = this.fb.group({
    id_unidad: new FormControl(),
    nombre_unidad: new FormControl(''),
    descripcion_unidad: new FormControl(''),
    id_empresa: new FormControl(sessionStorage.getItem("id_empresa"))
  })

  ngOnInit(): void {
    this.obtenerDataUnidades()
    this.verificarRolAdmin()
    this.authService.onCheckUser()
  }

  postForm(form: UnidadesI) {
    let unidadid = this.activerouter.snapshot.paramMap.get('id')

    Swal.fire({
      title: '¿Quieres guardar tus cambios??',
      showDenyButton: true,
      confirmButtonText: 'Confirmar',
      denyButtonText: `Cancelar`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        Swal.fire('Cambios realizados!', '', 'success')
        this.api.putUnidades(form, unidadid).subscribe(data => {
          let respuesta: UnidadesI = data
          this.router.navigate(['/listarUnidades'])
          
        })
      } else if (result.isDenied) {
        Swal.fire('Los cambios no fueron guardados', '', 'info')
        this.router.navigate(['/listarUnidades'])
      }
    })

  }

  obtenerDataUnidades(){
    let rolid = this.activerouter.snapshot.paramMap.get('id')

    this.api.getSingleUnidades(rolid).subscribe((data:any) => {
      
      this.datosUnidades = data
      this.editarForm.patchValue({
        "id_unidad": this.datosUnidades.id_unidad,
        "nombre_unidad": this.datosUnidades.nombre_unidad,
        "descripcion_unidad": this.datosUnidades.descripcion_unidad,
      })

    })
  }

  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  obtenerRol(){
    const rol_usuario = sessionStorage.getItem("rol_usuario")

    return rol_usuario
  }

  verificarRolAdmin(){
    if(this.obtenerRol() == "Administrador"){
      this.isAdministrador = true
      this.isFuncionario = true
    } else {
      Swal.fire(
        'Error',
        'No cuenta con los permisos para acceder a esta funcionalidad',
        'error'
      ).then((result) => {
        this.router.navigate(['/principal'])
    });
    }
    return this.obtenerRol()
  }

  //FIN configurando ROL

}
