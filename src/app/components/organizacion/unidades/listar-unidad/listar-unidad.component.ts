import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { UnidadesI } from 'src/app/models/organizacion/unidades.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listar-unidad',
  templateUrl: './listar-unidad.component.html',
  styleUrls: ['./listar-unidad.component.css']
})
export class ListarUnidadComponent implements OnInit {

  public unidades: Array<any> = []

  // NUEVO DATA TABLE

  // prueba new data table
  displayedColumns: string[] = ['id_unidad', 'nombre_unidad', 'descripcion_unidad', 'acciones'];

  dataSource!: MatTableDataSource<any>

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  public listUnidades: Array<UnidadesI> = [];

  constructor(private orgSvc: OrganizacionService, private router: Router, private dialogRef: MatDialog,
    public authService: AuthService, public alertas: AlertasService) { }

  // ORUEBA22
  sucription: Subscription | undefined;

  ngOnInit(): void {
    this.cargarUnidades()
    this.verificarRolAdmin()

    this.authService.onCheckUser()

    this.sucription = this.orgSvc.refresh.subscribe(() => {
      this.cargarUnidades()
    })

  }

  cargarUnidades() {
    let id= sessionStorage.getItem("id_empresa")
    this.orgSvc.getUnidades(id).subscribe((resp: any) => {
      // console.log(resp)
      this.listUnidades = resp
      this.dataSource = new MatTableDataSource(this.listUnidades)
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort
    })
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  eliminarUnidades(id: any) {
    Swal.fire({
      title: '¿Estas seguro?',
      text: "Esto eliminará el registro",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.orgSvc.deleteUnidades(id).subscribe(data => {
          if (result.isConfirmed) {
            Swal.fire(
              'Eliminado!',
              'Registro correctamente eliminado',
              'success'
            )
          }
          this.cargarUnidades()
        }, err => {
          let message = ""
          if (err.error.code = 23000) {
            message = "Esta Empresa tiene empleados"
          } else {
            message = "Error no controlado"
          }
          this.alertas.showFormError(message)
        })
      }
    })
  }

  editarUnidades(id:any){
    this.router.navigate(['editarUnidades/' + id])
  }

  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  verificarRolAdmin() {
    const rol_usuario = sessionStorage.getItem("rol_usuario")
    if (rol_usuario == "Administrador") {
      this.isAdministrador = true
      this.isFuncionario = true
    } else {
      this.alertas.errorPermisos()
    }
    return rol_usuario
  }

  //FIN configurando ROL

}
