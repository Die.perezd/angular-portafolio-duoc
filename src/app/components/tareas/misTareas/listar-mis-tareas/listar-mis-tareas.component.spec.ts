import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarMisTareasComponent } from './listar-mis-tareas.component';

describe('ListarMisTareasComponent', () => {
  let component: ListarMisTareasComponent;
  let fixture: ComponentFixture<ListarMisTareasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarMisTareasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarMisTareasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
