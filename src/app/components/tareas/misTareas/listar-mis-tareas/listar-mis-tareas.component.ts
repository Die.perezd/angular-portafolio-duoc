import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { MisTareasI, TareasI } from 'src/app/models/tareasYFlujos/tareas.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { TareasYFlujosService } from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listar-mis-tareas',
  templateUrl: './listar-mis-tareas.component.html',
  styleUrls: ['./listar-mis-tareas.component.css']
})
export class ListarMisTareasComponent implements OnInit {

  public areas: Array<any> = []
  // NUEVO DATA TABLE

  // prueba new data table
  displayedColumns: string[] = ['id_tarea', 'fecha_asignacion', 'fecha_plazo'
    , 'nombre_tarea', 'nombre_estado', 'indicador', 'acciones'];

  dataSource!: MatTableDataSource<any>

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  public listMisTareas: Array<MisTareasI> = [];

  constructor(private tyf: TareasYFlujosService, private router: Router, private dialogRef: MatDialog,
    public authService: AuthService, public alertas: AlertasService, public fb: FormBuilder) { }

  // ORUEBA22
  sucription: Subscription | undefined;

  ngOnInit(): void {
    this.cargarMisTareas()
    //this.cargarEstadosTareas()
    this.verificarRolAdmin()

    this.authService.onCheckUser()

    this.sucription = this.tyf.refresh.subscribe(() => {
      this.cargarMisTareas()
    })
  }

  progress(valor: number) {
    /* 
    const valor1: any[] = Object.values(this.listMisTareas)
    var valores = new Array()
    for (let x of valor1.values()) {
      console.log(x.id_estado_tarea)
      valores.push(x.id_estado_tarea)
    }
    */


    /* 
    let prueba = Object(this.listMisTareas).find((element: { id_estado_tarea: any; }) => element.id_estado_tarea)
    let estado = prueba.id_estado_tarea
    console.log("VALORRRR: ", estado)
    */
    return valor * 16.6
  }


  cargarMisTareas() {
    let id = sessionStorage.getItem("mi_id")
    this.tyf.getMisTareas(id).subscribe((resp: any) => {
      // console.log(resp)
      if (Object.entries(resp).length === 0) {
        console.log("NO HAY DATA")
        Swal.fire({
          title: 'NO HAY TAREAS',
          text: "No tienes tareas cargadas de momento",
          icon: 'warning',
        })
        this.router.navigate(['/principal'])
      } else {
        this.listMisTareas = resp

        /*
        for (let i of resp) {
          let prueba = resp.find((element: { id_estado_tarea: any; }) => element.id_estado_tarea)
          let estado =+ prueba.id_estado_tarea 
          console.log(i)
        }
         
        if (estado = 4) {
          console.log("ESTADO: TERMINADOOOO")
          let miDiv = document.getElementById("miEstado");
          let htmlString = `<h1>Encabezado1</h1>`
          miDiv.innerHTML = htmlString
        }
        */

        this.dataSource = new MatTableDataSource(this.listMisTareas)
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort
      }
    })
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  editarTarea(id: any) {
    this.router.navigate(['editarEstadoTarea/' + id])
  }

  crearSubTarea(id: any) {
    this.router.navigate(['crearSubTarea/' + id])
  }

  listarSubTareas(id: any) {
    this.router.navigate(['listarSubTarea/' + id])
  }


  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  verificarRolAdmin() {
    const rol_usuario = sessionStorage.getItem("rol_usuario")
    if (rol_usuario == "Administrador" || rol_usuario == "Funcionario" || rol_usuario == "Diseñador de flujos") {
      this.isAdministrador = true
      this.isFuncionario = true
      this.isDisenador = true
    } else {
      this.alertas.errorPermisos()
    }
    return rol_usuario
  }

  //FIN configurando ROL


}
