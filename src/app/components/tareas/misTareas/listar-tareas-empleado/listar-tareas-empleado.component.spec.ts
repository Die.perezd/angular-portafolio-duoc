import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarTareasEmpleadoComponent } from './listar-tareas-empleado.component';

describe('ListarTareasEmpleadoComponent', () => {
  let component: ListarTareasEmpleadoComponent;
  let fixture: ComponentFixture<ListarTareasEmpleadoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarTareasEmpleadoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarTareasEmpleadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
