import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DependenciaEmpleadoI } from 'src/app/models/tareas/dependencia.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service';
import { TareasYFlujosService } from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listar-equipo',
  templateUrl: './listar-equipo.component.html',
  styleUrls: ['./listar-equipo.component.css']
})
export class ListarEquipoComponent implements OnInit {

  public dependencia_empleado: Array<DependenciaEmpleadoI> = []
  // NUEVO DATA TABLE

  // prueba new data table
  displayedColumns: string[] = ['id_empleado_subordinado', 'nombres', 'acciones'];

  dataSource!: MatTableDataSource<any>

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  public listEmpleados: Array<DependenciaEmpleadoI> = [];
  public infoEmpleadoSubordinado: Array<DependenciaEmpleadoI> = [];


  constructor(private tyf: TareasYFlujosService, private router: Router, private dialogRef: MatDialog,
    public authService: AuthService, public alertas: AlertasService, private orgSvc: OrganizacionService,
    private activerouter: ActivatedRoute) { }

  // ORUEBA22
  sucription: Subscription | undefined;

  ngOnInit(): void {
    this.cargarListaDeEmpleados()
    this.verificarRolAdmin()

    this.authService.onCheckUser()

    this.sucription = this.tyf.refresh.subscribe(() => {
      this.cargarListaDeEmpleados()
    })

  }

  // Jugando con la dependencias de empleados
  cargarListaDeEmpleados() {
    let id = sessionStorage.getItem("mi_id")

    this.tyf.getDependenciaEmpleado(id).subscribe((resp: any) => {
      this.listEmpleados = resp

      if (Object.entries(resp).length === 0) {
        console.log("NO HAY DATA")
        Swal.fire({
          title: 'AVISO',
          text: "No tienes integrantes en tu equipo de trabajo, esta pantalla será visible cuando posea un equipo",
          icon: 'warning',
        })
        this.router.navigate(['/principal'])
      } else {
        this.dataSource = new MatTableDataSource(this.listEmpleados)
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort
      }

    })
  }

  getMiID() {
    let miID = sessionStorage.getItem("mi_id")
    return miID
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  eliminarTarea(id: any) {
    Swal.fire({
      title: '¿Estas seguro?',
      text: "Esto eliminará el registro",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.tyf.deleteTarea(id).subscribe(data => {
          if (result.isConfirmed) {
            Swal.fire(
              'Eliminado!',
              'Registro correctamente eliminado',
              'success'
            )
          }
          this.cargarListaDeEmpleados()
        })
      }
    })
  }

  editarTarea(id: any) {
    this.router.navigate(['editarTareas/' + id])
  }

  asignarTarea(id: any) {
    this.router.navigate(['asignarTareas/' + id])
  }

  asignarFlujo(id: any) {
    this.router.navigate(['asignarFlujo/' + id])
  }

  listarTareasEmpleado(id: any){
    this.router.navigate(['listarTareasEmpleado/' + id])
  }

  listarFlujosTrabajador(id:any){
    this.router.navigate(['listarFlujosMiEquipo/'+id])
  }

  /*
  verArea(id:any){
    this.router.navigate(['verArea/' + id])
  }
  */

  /*
  verArea(id:number){
    this.dialogRef.open(VerAreaComponent, {
      data: id
    })
  }
  */


  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  verificarRolAdmin() {
    const rol_usuario = sessionStorage.getItem("rol_usuario")
    if (rol_usuario == "Administrador" || rol_usuario == "Funcionario" || rol_usuario == 'Diseñador de flujos') {
      this.isAdministrador = true
      this.isFuncionario = true
      this.isDisenador = true
    } else {
      this.alertas.errorPermisos()
    }
    return rol_usuario
  }

  //FIN configurando ROL




}
