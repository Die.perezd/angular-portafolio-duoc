import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { DependenciaEmpleadoI } from 'src/app/models/tareas/dependencia.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service';
import { TareasYFlujosService } from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';

@Component({
  selector: 'app-crear-equipo',
  templateUrl: './crear-equipo.component.html',
  styleUrls: ['./crear-equipo.component.css']
})
export class CrearEquipoComponent implements OnInit {

  nuevoForm = this.fb.group({
    
    id_empleado_jefe: new FormControl(sessionStorage.getItem("mi_id")),
    id_empleado_subordinado: new FormControl(''),
  })

  public dataEmpleados: Array<any> = []

  constructor(private fb: FormBuilder, private activerouter: ActivatedRoute, private router: Router, private orgSvc: OrganizacionService,
    private dialogRef: MatDialog, public authService: AuthService, private alertas: AlertasService, public tyf:TareasYFlujosService) { }

  ngOnInit(): void {
    this.obtenerDataEmpleadosEmpresa()

    this.verificarRolAdmin()
    this.authService.onCheckUser()
  }

  postForm(form: DependenciaEmpleadoI) {
    if (this.nuevoForm.valid) {
      this.tyf.createDependenciaEmpleado(form).subscribe(data => {
        this.alertas.ShowSucces2()
        this.router.navigate(['/listarEquipo'])
      }, err => {
        let message = "<ul>"
        err.error.forEach((element: { field: string; message: string; }) => {
          message += "<li> " + element.message + "</li> <hr>"
        });
        message += "</ul>"
        this.alertas.showFormError(message)
        console.log("Este es una captura de error: ", err.error)
      })
    }
  }

  obtenerDataEmpleadosEmpresa(){
    let id= sessionStorage.getItem("id_empresa")
    this.orgSvc.getEmpleadosEmpresa(id).subscribe((resp:any) =>{
      this.dataEmpleados = resp
    })

  }

  getMiID(){
    let miID = sessionStorage.getItem("mi_id")
    return miID
  }


  get id_empleado_jefe() { return sessionStorage.getItem("mi_id") }
  get id_empleado_subordinado() { return this.nuevoForm.get('id_empleado_subordinado'); }
  

  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  verificarRolAdmin(){
    const rol_usuario = sessionStorage.getItem("rol_usuario")
    if(rol_usuario == "Administrador"){
      this.isAdministrador = true
      this.isFuncionario = true
    } else {
      this.alertas.errorPermisos()
    }
    return rol_usuario
  }

  //FIN configurando ROL

  
}
