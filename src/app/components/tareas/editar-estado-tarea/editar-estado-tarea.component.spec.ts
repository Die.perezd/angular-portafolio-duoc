import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarEstadoTareaComponent } from './editar-estado-tarea.component';

describe('EditarEstadoTareaComponent', () => {
  let component: EditarEstadoTareaComponent;
  let fixture: ComponentFixture<EditarEstadoTareaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarEstadoTareaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarEstadoTareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
