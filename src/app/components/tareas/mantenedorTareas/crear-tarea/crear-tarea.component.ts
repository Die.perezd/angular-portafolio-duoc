import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { TareasI } from 'src/app/models/tareasYFlujos/tareas.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service';
import { TareasYFlujosService } from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';

@Component({
  selector: 'app-crear-tarea',
  templateUrl: './crear-tarea.component.html',
  styleUrls: ['./crear-tarea.component.css']
})
export class CrearTareaComponent implements OnInit {

  
  nuevoForm = this.fb.group({
    nombre_tarea: new FormControl(),
    descripcion_tarea: new FormControl(''),
    id_empresa: new FormControl(sessionStorage.getItem("id_empresa"))
  })

  constructor(private fb:FormBuilder, private activerouter: ActivatedRoute, private router: Router, private tyf:TareasYFlujosService,
    private dialogRef: MatDialog, public authService: AuthService, private alertas: AlertasService) { }

  ngOnInit(): void {
    this.verificarRolAdmin()
    
    this.authService.onCheckUser()
  }

  closeDialog() {
    this.dialogRef.closeAll()
  }

  postForm(form: TareasI) {
    if (this.nuevoForm.valid) {
      this.tyf.createTarea(form).subscribe(data => {
        this.alertas.ShowSucces2()
        this.router.navigate(["/listarTareas"])
      }, err =>{
        let message= "<ul>"
        err.error.forEach((element: { field: string; message: string; }) => {
          message += "<li> " + element.message + "</li> <hr>"
        });
        message += "</ul>"
        this.alertas.showFormError(message)
        console.log("Este es una captura de error: ",err.error)
      })

    } else {
      let message = "<h2> Los campos no pueden ser vacíos </h2>"
      this.alertas.showFormError(message)
      console.log("Not Valid")
    }
  }

  salir() {
    this.dialogRef.closeAll()
    this.router.navigate(['listarColaboradores'])
  }

  // Obteniendo data del form
  get nombre_area() { return this.nuevoForm.get('nombre_area'); }
  get descripcion_area() { return this.nuevoForm.get('descripcion_area'); }

  
  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  verificarRolAdmin(){
    const rol_usuario = sessionStorage.getItem("rol_usuario")
    if(rol_usuario == "Administrador"){
      this.isAdministrador = true
      this.isFuncionario = true
    } else {
      this.alertas.errorPermisos()
    }
    return rol_usuario
  }

  //FIN configurando ROL

}
