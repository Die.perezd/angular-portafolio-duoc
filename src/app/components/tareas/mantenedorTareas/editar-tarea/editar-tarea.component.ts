import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TareasI } from 'src/app/models/tareasYFlujos/tareas.interface';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service';
import { TareasYFlujosService } from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-tarea',
  templateUrl: './editar-tarea.component.html',
  styleUrls: ['./editar-tarea.component.css']
})
export class EditarTareaComponent implements OnInit {
  constructor(private activerouter: ActivatedRoute, private router:Router, private api:TareasYFlujosService, private fb: FormBuilder,
    public authService:AuthService) { }

  datosTareas!: TareasI;
  editarForm = this.fb.group({
    id_tarea: new FormControl(),
    nombre_tarea: new FormControl(''),
    descripcion_tarea: new FormControl(''),
    id_empresa: new FormControl(sessionStorage.getItem("id_empresa"))
  })

  ngOnInit(): void {
    this.obtenerDataTareas()
    this.verificarRolAdmin()
    this.authService.onCheckUser()
  }

  postForm(form: TareasI) {
    let tareaid = this.activerouter.snapshot.paramMap.get('id')

    Swal.fire({
      title: '¿Quieres guardar tus cambios??',
      showDenyButton: true,
      confirmButtonText: 'Confirmar',
      denyButtonText: `Cancelar`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        Swal.fire('Cambios realizados!', '', 'success')
        this.api.putTarea(form, tareaid).subscribe(data => {
          let respuesta: TareasI = data
          this.router.navigate(['/listarTareas'])
          
        })
      } else if (result.isDenied) {
        Swal.fire('Los cambios no fueron guardados', '', 'info')
        this.router.navigate(['/listarTareas'])
      }
    })

  }

  obtenerDataTareas(){
    let tareaid = this.activerouter.snapshot.paramMap.get('id')

    this.api.getSingleTarea(tareaid).subscribe((data:any) => {
      
      this.datosTareas = data
      this.editarForm.patchValue({
        "id_tarea": this.datosTareas.id_tarea,
        "nombre_tarea": this.datosTareas.nombre_tarea,
        "descripcion_tarea": this.datosTareas.descripcion_tarea,
      })

    })
  }

  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  obtenerRol(){
    const rol_usuario = sessionStorage.getItem("rol_usuario")

    return rol_usuario
  }

  verificarRolAdmin(){
    if(this.obtenerRol() == "Administrador"){
      this.isAdministrador = true
      this.isFuncionario = true
    } else {
      Swal.fire(
        'Error',
        'No cuenta con los permisos para acceder a esta funcionalidad',
        'error'
      ).then((result) => {
        this.router.navigate(['/principal'])
    });
    }
    return this.obtenerRol()
  }

}
