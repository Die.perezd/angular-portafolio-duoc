import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { DependenciaTareaI } from 'src/app/models/tareas/dependenciaTarea.interface';
import { TareasI } from 'src/app/models/tareasYFlujos/tareas.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service';
import { TareasYFlujosService } from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-asignar-tarea',
  templateUrl: './asignar-tarea.component.html',
  styleUrls: ['./asignar-tarea.component.css']
})
export class AsignarTareaComponent implements OnInit {

  public tareas: Array<any> = []

  constructor(private activerouter: ActivatedRoute, private router: Router, private api: TareasYFlujosService, private fb: FormBuilder,
    public authService: AuthService, private orgSvc: OrganizacionService, public alertas:AlertasService) { }


  date = new Date()
  datosTareasEmpleado!: DependenciaTareaI;
  nuevoForm = this.fb.group({
    fecha_plazo: new FormControl(''),
    fecha_asignacion: new FormControl(this.date),
    id_empresa: new FormControl(sessionStorage.getItem("id_empresa")),
    id_tarea: new FormControl(''),
    id_empleado_responsable: new FormControl(this.getIDEmpleadoSeleccionado()),
    id_empleado_jefatura: new FormControl(sessionStorage.getItem("mi_id")),
    id_estado_tarea: new FormControl(1)
  })

  public dataEmpleados: Array<any> = []

  ngOnInit(): void {
    this.obtenerData()
    this.obtenerDataTareas()
    this.obtenerDataEmpleadosEmpresa()
    this.verificarRolAdmin()
    this.authService.onCheckUser()
  }

  postForm(form: DependenciaTareaI) {
    if (this.nuevoForm.valid) {
      this.api.createDependenciaTareas(form).subscribe(data => {
        this.alertas.ShowSucces2()
        this.router.navigate(["/listarEquipo"])
      }, err =>{
        let message= "<ul>"
        err.error.forEach((element: { field: string; message: string; }) => {
          message += "<li> " + element.message + "</li> <hr>"
        });
        message += "</ul>"
        this.alertas.showFormError(message)
        console.log("Este es una captura de error: ",err.error)
      })

    } else {
      let message = "<h2> Los campos no pueden ser vacíos </h2>"
      this.alertas.showFormError(message)
      console.log("Not Valid")
    }
  }

  obtenerData() {
    let id = sessionStorage.getItem("id_empresa")
    this.api.getTareas(id).subscribe((resp: any) => {
      this.tareas = resp
      console.log(this.tareas)
    })
  }

  obtenerDataTareas() {
    let tareaid = this.activerouter.snapshot.paramMap.get('id')

    this.api.getSingleTarea(tareaid).subscribe((data: any) => {

      this.datosTareasEmpleado = data
      this.nuevoForm.patchValue({
        id_empresa: new FormControl(sessionStorage.getItem("id_empresa")),
        "fecha_plazo": this.datosTareasEmpleado.fecha_plazo,
        "fecha_asignacion": this.datosTareasEmpleado.fecha_asignacion,
        "id_tarea": this.datosTareasEmpleado.id_tarea,
        "id_empleado_responsable": this.datosTareasEmpleado.id_empleado_responsable,
        "id_empleado_jefatura": this.datosTareasEmpleado.id_empleado_jefatura,
        "id_estado_tarea": this.datosTareasEmpleado.id_estado_tarea
      })

    })
  }

  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  obtenerRol() {
    const rol_usuario = sessionStorage.getItem("rol_usuario")

    return rol_usuario
  }

  verificarRolAdmin() {
    if (this.obtenerRol() == "Administrador" || this.obtenerRol() == "Funcionario" || this.obtenerRol() == "Diseñador de flujos") {
      this.isAdministrador = true
      this.isFuncionario = true
      this.isDisenador = true
    } else {
      Swal.fire(
        'Error',
        'No cuenta con los permisos para acceder a esta funcionalidad',
        'error'
      ).then((result) => {
        this.router.navigate(['/principal'])
      });
    }
    return this.obtenerRol()
  }


  // JUGANDO CON LA ASIGNACION
  getMiID() {
    let miID = sessionStorage.getItem("mi_id")
    return miID
  }

  getIDEmpleadoSeleccionado(){
    let po = this.activerouter.snapshot.paramMap.get('id')
    return po
  }

  get id_empleado_jefatura() { return sessionStorage.getItem("mi_id") }
  get fecha_plazo() { return this.nuevoForm.get('fecha_plazo'); }
  get fecha_asignacion() { return this.nuevoForm.get('fecha_asignacion'); }
  get id_empleado_responsable() { return this.getIDEmpleadoSeleccionado() }

  obtenerDataEmpleadosEmpresa() {
    let id = sessionStorage.getItem("id_empresa")
    this.orgSvc.getEmpleadosEmpresa(id).subscribe((resp: any) => {
      this.dataEmpleados = resp
    })

  }

}
