import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SubTareaI } from 'src/app/models/tareas/dependenciaTarea.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { TareasYFlujosService } from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listar-sub-tareas',
  templateUrl: './listar-sub-tareas.component.html',
  styleUrls: ['./listar-sub-tareas.component.css']
})
export class ListarSubTareasComponent implements OnInit {

  public subTareas: Array<any> = []
  // NUEVO DATA TABLE

  // prueba new data table
  displayedColumns: string[] = ['id_subtarea', 'nombre_tarea_padre', 'nombre_subtarea', 'descripcion_subtarea',
    'fecha_plazo', 'estado', 'acciones'];

  dataSource!: MatTableDataSource<any>

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  public listSubTareas: Array<SubTareaI> = [];

  constructor(private tyf: TareasYFlujosService, private router: Router, private activeRouter: ActivatedRoute,
    public authService: AuthService, public alertas: AlertasService, public fb: FormBuilder) { }

  // ORUEBA22
  sucription: Subscription | undefined;

  ngOnInit(): void {
    this.cargarSubTareas()
    this.cargarEstadosSubTareas()
    //this.cargarEstadosTareas()
    this.verificarRolAdmin()

    this.authService.onCheckUser()

    this.sucription = this.tyf.refresh.subscribe(() => {
      this.cargarSubTareas()
    })
  }

  nombre_tarea_padre: any

  cargarSubTareas() {
    let id = this.activeRouter.snapshot.paramMap.get('id')
    this.tyf.getSubTarea(id).subscribe((resp: any) => {
      // console.log(resp)
      if (Object.entries(resp).length === 0) {
        console.log("NO HAY DATA")
        Swal.fire({
          title: 'NO HAY TAREAS',
          text: "No tienes tareas cargadas de momento",
          icon: 'warning',
        })
        this.router.navigate(['/principal'])
      } else {
        this.listSubTareas = resp
        console.log("PRUEBA:",this.listSubTareas)
        /* Falta agregar que filtre por id_tarea_empleado 
        let prueba = resp.find((element: { tareaEmpleado: any; }) => element.tareaEmpleado.tarea.nombre_tarea)
        this.nombre_tarea_padre = prueba.tareaEmpleado.tarea.nombre_tarea
        console.log(this.nombre_tarea_padre)
        */
        this.dataSource = new MatTableDataSource(this.listSubTareas)
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort
      }
    })
  }

  public estados: Array<any> = []

  cargarEstadosSubTareas() {
    this.tyf.getEstadosSubTareas().subscribe((resp: any) => {
      this.estados = resp
      console.log("AA:",this.estados)
    })
  }

  eliminarSubTarea(id:any) {
    Swal.fire({
      title: '¿Estas seguro?',
      text: "Esto eliminará el registro",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.tyf.deleteSubTarea(id).subscribe(data => {
          if (result.isConfirmed) {
            Swal.fire(
              'Eliminado!',
              'Registro correctamente eliminado',
              'success'
            )
          }
          this.cargarSubTareas()
        })
      }
    })
  }

  editarSubTarea(id:any, id2:any){
    this.router.navigate(['editarSubTarea/' + id +'/'+id2])
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  verificarRolAdmin() {
    const rol_usuario = sessionStorage.getItem("rol_usuario")
    if (rol_usuario == "Administrador" || rol_usuario == "Funcionario" || rol_usuario == "Diseñador de flujos") {
      this.isAdministrador = true
      this.isFuncionario = true
      this.isDisenador = true
    } else {
      this.alertas.errorPermisos()
    }
    return rol_usuario
  }

  //FIN configurando ROL

}
