import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarSubTareasComponent } from './listar-sub-tareas.component';

describe('ListarSubTareasComponent', () => {
  let component: ListarSubTareasComponent;
  let fixture: ComponentFixture<ListarSubTareasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarSubTareasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarSubTareasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
