import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DependenciaTareaI, SubTareaI } from 'src/app/models/tareas/dependenciaTarea.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service';
import { TareasYFlujosService } from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-crear-subtarea',
  templateUrl: './crear-subtarea.component.html',
  styleUrls: ['./crear-subtarea.component.css']
})
export class CrearSubtareaComponent implements OnInit {

  public tareas: Array<any> = []

  constructor(private activerouter: ActivatedRoute, private router: Router, private api: TareasYFlujosService, private fb: FormBuilder,
    public authService: AuthService, private orgSvc: OrganizacionService, public alertas:AlertasService) { }


  date = new Date()
  datosSubTareas!: SubTareaI;
  nuevoForm = this.fb.group({
    nombre_subtarea: new FormControl(''),
    descripcion_subtarea: new FormControl(''),
    fecha_plazo: new FormControl(''),
    id_tarea_empleado: new FormControl(this.activerouter.snapshot.paramMap.get('id')),
    id_empresa: new FormControl(sessionStorage.getItem("id_empresa")),
    id_estado_tarea: new FormControl(2),
  })

  public dataEmpleados: Array<any> = []

  ngOnInit(): void {
    this.obtenerData()
    this.obtenerDataEmpleadosEmpresa()
    this.verificarRolAdmin()
    this.authService.onCheckUser()
  }

  postForm(form: SubTareaI) {
    if (this.nuevoForm.valid) {

      // aqui implementar url para crear la subtarea
      this.api.createSubTarea(form).subscribe(data => {
        this.alertas.ShowSucces2()
        this.router.navigate(["/principal"])
      }, err =>{
        let message= "<ul>"
        err.error.forEach((element: { field: string; message: string; }) => {
          message += "<li> " + element.message + "</li> <hr>"
        });
        message += "</ul>"
        this.alertas.showFormError(message)
        console.log("Este es una captura de error: ",err.error)
      })

    } else {
      let message = "<h2> Los campos no pueden ser vacíos </h2>"
      this.alertas.showFormError(message)
      console.log("Not Valid")
    }
  }

  obtenerData() {
    let id = sessionStorage.getItem("id_empresa")
    this.api.getTareas(id).subscribe((resp: any) => {
      this.tareas = resp
      console.log("RESP, ",resp)
      //let prueba = resp.find((element: { prueba: any; }) => element.prueba.tareaEmpleado)
      //const nombre = prueba.tareaEmpleado.tarea.nombre_tarea
      //console.log(prueba)
      this.datosSubTareas = resp
      this.nuevoForm.patchValue({
        // "id_empresa": new FormControl(sessionStorage.getItem("id_empresa")),
        "nombre_subtarea": this.datosSubTareas.nombre_subtarea,
        "descripcion_subtarea": this.datosSubTareas.descripcion_subtarea,
        "fecha_plazo": this.datosSubTareas.fecha_plazo,
      })
    })
  }


  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  obtenerRol() {
    const rol_usuario = sessionStorage.getItem("rol_usuario")

    return rol_usuario
  }

  verificarRolAdmin() {
    if (this.obtenerRol() == "Administrador" || this.obtenerRol() == "Funcionario" || this.obtenerRol() == "Diseñador de flujos") {
      this.isAdministrador = true
      this.isFuncionario = true
    } else {
      Swal.fire(
        'Error',
        'No cuenta con los permisos para acceder a esta funcionalidad',
        'error'
      ).then((result) => {
        this.router.navigate(['/principal'])
      });
    }
    return this.obtenerRol()
  }


  // JUGANDO CON LA ASIGNACION
  getMiID() {
    let miID = sessionStorage.getItem("mi_id")
    return miID
  }

  getIDEmpleadoSeleccionado(){
    let po = this.activerouter.snapshot.paramMap.get('id')
    return po
  }

  get id_empleado_jefatura() { return sessionStorage.getItem("mi_id") }
  get fecha_plazo() { return this.nuevoForm.get('fecha_plazo'); }
  get fecha_asignacion() { return this.nuevoForm.get('fecha_asignacion'); }
  get id_empleado_responsable() { return this.getIDEmpleadoSeleccionado() }

  obtenerDataEmpleadosEmpresa() {
    let id = sessionStorage.getItem("id_empresa")
    this.orgSvc.getEmpleadosEmpresa(id).subscribe((resp: any) => {
      this.dataEmpleados = resp
    })

  }

}
