import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { SubTareaI } from 'src/app/models/tareas/dependenciaTarea.interface';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { TareasYFlujosService } from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-subtarea',
  templateUrl: './editar-subtarea.component.html',
  styleUrls: ['./editar-subtarea.component.css']
})
export class EditarSubtareaComponent implements OnInit {

  constructor(private activerouter: ActivatedRoute, private router: Router, private api: TareasYFlujosService, private fb: FormBuilder,
    public authService: AuthService) { }

  public tareas: Array<any> = []

  datosAreas!: SubTareaI;
  editarForm = this.fb.group({
    id_empresa: new FormControl(sessionStorage.getItem("id_empresa")),
    fecha_plazo: new FormControl(''),
    id_estado_subtarea: new FormControl(''),
    id_tarea_empleado: new FormControl(this.activerouter.snapshot.paramMap.get('id2'))
  })

  sucription: Subscription | undefined;
  ngOnInit(): void {
    //this.obtenerData()
    this.obtenerDataTareas()
    this.cargarEstadosTareas()
    this.verificarRolAdmin()
    this.authService.onCheckUser()

  }

  // CAMBIANDO EL ESTADO DE LA TAREA
  postForm(form: SubTareaI) {
    let areaid = this.activerouter.snapshot.paramMap.get('id')

    Swal.fire({
      title: '¿Quieres guardar tus cambios??',
      showDenyButton: true,
      confirmButtonText: 'Confirmar',
      denyButtonText: `Cancelar`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        Swal.fire('Cambios realizados!', '', 'success')
        this.api.putSubTarea(form, areaid).subscribe(data => {
          let respuesta: SubTareaI = data
          this.router.navigate(['/listarMisTareas'])

        })
      } else if (result.isDenied) {
        Swal.fire('Los cambios no fueron guardados', '', 'info')
        this.router.navigate(['/listarMisTareas'])
      }
    })

  }

  /*
  obtenerData() {
    let id = sessionStorage.getItem("mi_id")
    this.api.getMisTareas(id).subscribe((resp: any) => {
      this.tareas = resp
      let prueba = resp.find((element: { fecha_asignacion: any; }) => element.fecha_asignacion)
      console.log("MIOOOOOO ", prueba)
      this.editarForm.get('fecha_asignacion')?.setValue(prueba.fecha_asignacion)
      this.editarForm.get('fecha_plazo')?.setValue(prueba.fecha_plazo)
      this.editarForm.get('id_tarea')?.setValue(prueba.id_tarea)
      this.editarForm.get('id_tarea_empleado')?.setValue(prueba.id_tarea_empleado)
    })
  }
  */

  obtenerDataTareas() {
    let id = this.activerouter.snapshot.paramMap.get('id2')

    this.api.getSubTarea(id).subscribe((data: any) => {
      console.log("PROBANDO ", data)
      this.tareas = data
      
      let prueba = data.find((element: { fecha_plazo: any; }) => element.fecha_plazo)
      console.log("MIOOOOOO ", prueba)
      this.editarForm.get('fecha_plazo')?.setValue(prueba.fecha_plazo)
      
     
      this.datosAreas = data
      this.editarForm.patchValue({
        "id_estado_subtarea": this.datosAreas.id_estado_subtarea,
      })

    })
  }

  public estados: Array<any> = []

  cargarEstadosTareas() {
    this.api.getEstadosSubTareas().subscribe((resp: any) => {
      this.estados = resp
      console.log(this.estados)
    })
  }
 


  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  obtenerRol() {
    const rol_usuario = sessionStorage.getItem("rol_usuario")

    return rol_usuario
  }

  verificarRolAdmin() {
    if (this.obtenerRol() == "Administrador") {
      this.isAdministrador = true
      this.isFuncionario = true
    } else {
      Swal.fire(
        'Error',
        'No cuenta con los permisos para acceder a esta funcionalidad',
        'error'
      ).then((result) => {
        this.router.navigate(['/principal'])
      });
    }
    return this.obtenerRol()
  }

}
