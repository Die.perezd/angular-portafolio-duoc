import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarSubtareaComponent } from './editar-subtarea.component';

describe('EditarSubtareaComponent', () => {
  let component: EditarSubtareaComponent;
  let fixture: ComponentFixture<EditarSubtareaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarSubtareaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarSubtareaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
