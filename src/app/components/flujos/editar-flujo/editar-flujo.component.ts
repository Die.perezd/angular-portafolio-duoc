import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { FlujosDeTrabajoI } from 'src/app/models/tareasYFlujos/flujosDeTrabajo.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service';
import { TareasYFlujosService } from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-flujo',
  templateUrl: './editar-flujo.component.html',
  styleUrls: ['./editar-flujo.component.css']
})
export class EditarFlujoComponent implements OnInit {

  public data: Array<any> = []
  public empresas: Array<any> = []

  datosFlujosDeTrabajo!: FlujosDeTrabajoI;
  editarForm = this.fb.group({
    id_flujo_trabajo: new FormControl(''),
    nombre_flujo_trabajo: new FormControl(''),
    descripcion_flujo_trabajo: new FormControl(''),
    id_empresa: new FormControl(sessionStorage.getItem("id_empresa"))
  })

  constructor(private fb: FormBuilder, private activerouter: ActivatedRoute, private router: Router, private orgSvc: OrganizacionService,
    private dialogRef: MatDialog, public authService: AuthService, private alertas: AlertasService, public tyf: TareasYFlujosService) { }

  // NUEVO INTENTO DE CREAR EMPLEADO - EMPRESA

  ngOnInit(): void {
    this.authService.onCheckUser()
    this.verificarRolAdmin()
    this.obtenerFlujos()
  }

  postForm(form: FlujosDeTrabajoI) {
    let flujoid = this.activerouter.snapshot.paramMap.get('id')

    Swal.fire({
      title: '¿Quieres guardar tus cambios??',
      showDenyButton: true,
      confirmButtonText: 'Confirmar',
      denyButtonText: `Cancelar`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        Swal.fire('Cambios realizados!', '', 'success')
        this.tyf.putFlujosDeTrabajo(form, flujoid).subscribe(data => {
          let respuesta: FlujosDeTrabajoI = data
          this.router.navigate(['/listarFlujos'])
          
        })
      } else if (result.isDenied) {
        Swal.fire('Los cambios no fueron guardados', '', 'info')
        this.router.navigate(['/listarFlujos'])
      }
    })

  }

  obtenerFlujos() {
    let flujoid = this.activerouter.snapshot.paramMap.get('id')

    this.tyf.getSingleFlujosDeTrabajo(flujoid).subscribe((data: any) => {
      console.log("PROBANDO ", data)
      this.data = data

      this.datosFlujosDeTrabajo = data
      this.editarForm.patchValue({
        "id_flujo_trabajo": this.datosFlujosDeTrabajo.id_flujo_trabajo,
        "nombre_flujo_trabajo": this.datosFlujosDeTrabajo.nombre_flujo_trabajo,
        "descripcion_flujo_trabajo": this.datosFlujosDeTrabajo.descripcion_flujo_trabajo,
      })

    })
  }

   // configurando rol y alerta 
   isAdministrador = false
   isFuncionario = false
   isDisenador = false
 
   verificarRolAdmin() {
     const rol_usuario = sessionStorage.getItem("rol_usuario")
     if (rol_usuario == "Administrador" || rol_usuario == "Diseñador" ) {
       this.isAdministrador = true
       this.isDisenador = true
     } else {
       this.alertas.errorPermisos()
     }
     return rol_usuario
   }
   //FIN configurando ROL

}
