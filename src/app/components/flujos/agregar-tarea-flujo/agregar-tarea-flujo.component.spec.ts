import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarTareaFlujoComponent } from './agregar-tarea-flujo.component';

describe('AgregarTareaFlujoComponent', () => {
  let component: AgregarTareaFlujoComponent;
  let fixture: ComponentFixture<AgregarTareaFlujoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgregarTareaFlujoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarTareaFlujoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
