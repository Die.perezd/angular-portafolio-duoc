import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { TareaFlujoI } from 'src/app/models/tareasYFlujos/flujosDeTrabajo.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service';
import { TareasYFlujosService } from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';

@Component({
  selector: 'app-agregar-tarea-flujo',
  templateUrl: './agregar-tarea-flujo.component.html',
  styleUrls: ['./agregar-tarea-flujo.component.css']
})
export class AgregarTareaFlujoComponent implements OnInit {

  public data: Array<any> = []
  public empresas: Array<any> = []
  public flujos: Array<any> = []
  public tareas: Array<any> = []

  datosFlujosDeTrabajo!: TareaFlujoI;
  nuevoForm = this.fb.group({
    id_tarea_flujo: new FormControl(''),
    id_flujo_trabajo: new FormControl(this.activerouter.snapshot.paramMap.get('id')),
    id_tarea: new FormControl(''),
  })
  //CUSTOM DATE
  minDate: Date;
  maxDate: Date;

  constructor(private fb: FormBuilder, private activerouter: ActivatedRoute, private router: Router, private orgSvc: OrganizacionService,
    private dialogRef: MatDialog, public authService: AuthService, private alertas: AlertasService, public tyf: TareasYFlujosService) {
    // Set the minimum to January 1st 20 years in the past and December 31st a year in the future.
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 0, 0, 1);
    this.maxDate = new Date(currentYear + 1, 11, 31);
  }

  // NUEVO INTENTO DE CREAR EMPLEADO - EMPRESA

  

  ngOnInit(): void {
    this.obtenerData()
    this.obtenerEmpresas()
    this.obtenerFlujos()
    this.obtenerTareas()
    this.authService.onCheckUser()
    this.verificarRolAdmin()
  }

  postForm(form: TareaFlujoI) {
    if (this.nuevoForm.valid) {
      this.tyf.addTareaFlujo(form).subscribe(data => {
        this.alertas.ShowSucces2()
        this.router.navigate(['/listarFlujos'])
      }, err => {
        let message = "<ul>"
        err.error.forEach((element: { field: string; message: string; }) => {
          message += "<li> " + element.message + "</li> <hr>"
        });
        message += "</ul>"
        this.alertas.showFormError(message)
        console.log("Este es una captura de error: ", err.error)
      })
    }
  }


  obtenerData() {
    this.tyf.getTareasFlujoDeTrabajo().subscribe((resp: any) => {
      this.data = resp
      console.log(this.data)
    })
  }

  obtenerEmpresas() {
    this.orgSvc.getEmpresas().subscribe((resp: any) => {
      this.empresas = resp
      console.log(this.empresas)
    })
  }

  obtenerFlujos() {
    let id = sessionStorage.getItem("id_empresa")
    this.tyf.getFlujosDeTrabajo(id).subscribe((resp: any) => {
      this.flujos = resp
    })
  }

  obtenerTareas() {
    let id= sessionStorage.getItem("id_empresa")
    this.tyf.getTareas(id).subscribe((resp: any) => {
      this.tareas = resp
    })
  }

  get id_tarea_flujo() { return this.nuevoForm.get('id_tarea_flujo'); }
  get id_tarea() { return this.nuevoForm.get('id_tarea'); }
  get plazo() { return this.nuevoForm.get('plazo'); }

  /*
  toppings = new FormControl();

  toppingList: string[] = ['Extra cheese', 'Mushroom', 'Onion', 'Pepperoni', 'Sausage', 'Tomato'];
  */

  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  verificarRolAdmin() {
    const rol_usuario = sessionStorage.getItem("rol_usuario")
    if (rol_usuario == "Administrador" || rol_usuario == "Diseñador") {
      this.isAdministrador = true
      this.isDisenador = true
    } else {
      this.alertas.errorPermisos()
    }
    return rol_usuario
  }
  //FIN configurando ROL

}
