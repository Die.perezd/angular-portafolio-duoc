import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarTareaFlujoComponent } from './listar-tarea-flujo.component';

describe('ListarTareaFlujoComponent', () => {
  let component: ListarTareaFlujoComponent;
  let fixture: ComponentFixture<ListarTareaFlujoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarTareaFlujoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarTareaFlujoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
