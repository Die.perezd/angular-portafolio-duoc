import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FlujosDeTrabajoI } from 'src/app/models/tareasYFlujos/flujosDeTrabajo.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { TareasYFlujosService } from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listar-flujo',
  templateUrl: './listar-flujo.component.html',
  styleUrls: ['./listar-flujo.component.css']
})
export class ListarFlujoComponent implements OnInit {

  public flujosDeTrabajo:Array<any> = []

  // NUEVO DATA TABLE

  // prueba new data table
  displayedColumns: string[] = ['id_flujo', 'nombre_flujo', 'descripcion_flujo', 'acciones'];

  dataSource!: MatTableDataSource<any>

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  public listFlujosDeTrabajos: Array<FlujosDeTrabajoI> = [];

  constructor(private tyf: TareasYFlujosService, private router: Router, private dialogRef: MatDialog,
    public authService: AuthService, public alertas: AlertasService) { }

  // ORUEBA22
  sucription: Subscription | undefined;

  ngOnInit(): void {
    this.cargarFlujosDeTrabajo()
    this.verificarRolAdmin()

    this.authService.onCheckUser()

    this.sucription = this.tyf.refresh.subscribe(() => {
      this.cargarFlujosDeTrabajo()
    })

  }

  cargarFlujosDeTrabajo() {
    let id = sessionStorage.getItem("id_empresa")
    this.tyf.getFlujosDeTrabajo(id).subscribe((resp: any) => {
      // console.log(resp)
      this.listFlujosDeTrabajos = resp
      this.dataSource = new MatTableDataSource(this.listFlujosDeTrabajos)
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort
    })
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  eliminarFlujosDeTrabajos(id: any) {
    Swal.fire({
      title: '¿Estas seguro?',
      text: "Esto eliminará el registro",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.tyf.deleteFlujosDeTrabajo(id).subscribe(data => {
          if (result.isConfirmed) {
            Swal.fire(
              'Eliminado!',
              'Registro correctamente eliminado',
              'success'
            )
          }
          this.cargarFlujosDeTrabajo()
        }, err => {
          let message = ""
          if (err.error.code = 23000) {
            message = "Esta Tarea tiene empleados Asociados"
          } else {
            message = "Error no controlado"
          }
          this.alertas.showFormError(message)
        })
      }
    })
  }

  editarFlujosDeTrabajos(id:any){
    this.router.navigate(['editarFlujo/' + id])
  }

  crearTareaFlujo(id:any){
    this.router.navigate(['agregarTareaFlujo/' + id])
  }

  listarTareasFlujo(id:any, flujo:any){
    this.router.navigate(['listarTareasFlujo/' + id + '/' + flujo])
  }
  

  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  verificarRolAdmin() {
    const rol_usuario = sessionStorage.getItem("rol_usuario")
    if (rol_usuario == "Administrador" || rol_usuario == "Diseñador de flujos" ) {
      this.isAdministrador = true
      this.isDisenador = true
    } else {
      this.alertas.errorPermisos()
    }
    return rol_usuario
  }
  //FIN configurando ROL

}
