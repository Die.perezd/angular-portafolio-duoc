import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarFlujoComponent } from './listar-flujo.component';

describe('ListarFlujoComponent', () => {
  let component: ListarFlujoComponent;
  let fixture: ComponentFixture<ListarFlujoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarFlujoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarFlujoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
