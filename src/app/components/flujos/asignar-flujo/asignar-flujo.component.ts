import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FlujoEmpleadoI } from 'src/app/models/tareasYFlujos/flujosDeTrabajo.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service';
import { TareasYFlujosService } from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-asignar-flujo',
  templateUrl: './asignar-flujo.component.html',
  styleUrls: ['./asignar-flujo.component.css']
})
export class AsignarFlujoComponent implements OnInit {
  public tareas: Array<any> = []
  public flujos: Array<any> = []

  constructor(private activerouter: ActivatedRoute, private router: Router, private api: TareasYFlujosService, private fb: FormBuilder,
    public authService: AuthService, private orgSvc: OrganizacionService, public alertas: AlertasService) { }


  date = new Date()
  datosTareasEmpleado!: FlujoEmpleadoI;
  nuevoForm = this.fb.group({
    fecha_plazo: new FormControl(''),
    id_tarea_flujo: new FormControl(''),
    id_flujo_trabajo: new FormControl(''),
    id_estado_tarea_flujo_empleado: new FormControl(''),
    id_empleado_responsable: new FormControl(this.getIDEmpleadoSeleccionado()),
    id_empleado_jefatura: new FormControl(sessionStorage.getItem("mi_id")),
    id_empresa: new FormControl(sessionStorage.getItem("id_empresa")),
    
  })

  public dataEmpleados: Array<any> = []

  ngOnInit(): void {
    this.getFlujos()
    this.obtenerDataEmpleadosEmpresa()
    this.verificarRolAdmin()
    this.authService.onCheckUser()
  }

  postForm(form: FlujoEmpleadoI) {
    if (this.nuevoForm.valid) {
      this.api.asignarFlujoEmpleado(form).subscribe(data => {
        this.alertas.ShowSucces2()
        this.router.navigate(["/listarEquipo"])
      }, err => {
        let message = "<ul>"
        err.error.forEach((element: { field: string; message: string; }) => {
          message += "<li> " + element.message + "</li> <hr>"
        });
        message += "</ul>"
        this.alertas.showFormError(message)
        console.log("Este es una captura de error: ", err.error)
      })

    } else {
      let message = "<h2> Los campos no pueden ser vacíos </h2>"
      this.alertas.showFormError(message)
      console.log("Not Valid")
    }
  }

  getFlujos(){
    let id = sessionStorage.getItem("id_empresa")
    this.api.getFlujosDeTrabajo(id).subscribe((resp: any) => {
      this.flujos = resp
      console.log("FLUJOS: ",this.flujos)
    })
  }

  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  obtenerRol() {
    const rol_usuario = sessionStorage.getItem("rol_usuario")

    return rol_usuario
  }

  verificarRolAdmin() {
    if (this.obtenerRol() == "Administrador") {
      this.isAdministrador = true
      this.isFuncionario = true
    } else {
      Swal.fire(
        'Error',
        'No cuenta con los permisos para acceder a esta funcionalidad',
        'error'
      ).then((result) => {
        this.router.navigate(['/principal'])
      });
    }
    return this.obtenerRol()
  }


  // JUGANDO CON LA ASIGNACION
  getMiID() {
    let miID = sessionStorage.getItem("mi_id")
    return miID
  }

  getIDEmpleadoSeleccionado() {
    let po = this.activerouter.snapshot.paramMap.get('id')
    return po
  }

  get fecha_plazo() { return this.nuevoForm.get('fecha_plazo'); }
  get id_flujo_trabajo() { return this.nuevoForm.get('id_flujo_trabajo'); }

  obtenerDataEmpleadosEmpresa() {
    let id = sessionStorage.getItem("id_empresa")
    this.orgSvc.getEmpleadosEmpresa(id).subscribe((resp: any) => {
      this.dataEmpleados = resp
    })

  }

}
