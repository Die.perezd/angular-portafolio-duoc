import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { TareaFlujoI } from 'src/app/models/tareasYFlujos/flujosDeTrabajo.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { TareasYFlujosService } from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listar-tareas-mi-flujo',
  templateUrl: './listar-tareas-mi-flujo.component.html',
  styleUrls: ['./listar-tareas-mi-flujo.component.css']
})
export class ListarTareasMiFlujoComponent implements OnInit {

  public tareasMiFlujo: Array<any> = []

  // NUEVO DATA TABLE

  // prueba new data table
  displayedColumns: string[] = ['nombres_jefatura', 'fecha_plazo'
  ,'nombre_tarea','descripcion_tarea','nombre_estado', 'acciones'];

  dataSource!: MatTableDataSource<any>

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  public listTareasFlujo: Array<TareaFlujoI> = [];

  constructor(private tyf: TareasYFlujosService, private router: Router, private dialogRef: MatDialog,
    public authService: AuthService, public alertas: AlertasService, private activerouter: ActivatedRoute) { }

  // ORUEBA22
  sucription: Subscription | undefined;

  ngOnInit(): void {
    this.cargarTareasFlujo()
    this.verificarRolAdmin()

    this.authService.onCheckUser()

    this.sucription = this.tyf.refresh.subscribe(() => {
      this.cargarTareasFlujo()
    })

  }

  getFechaAsignacionFlujo(){
    var fecha_asignacion_flujo = this.activerouter.snapshot.paramMap.get('idFecha')
    return fecha_asignacion_flujo
  }

  getNombreResponsable(){
    var nombre_responsable = this.activerouter.snapshot.paramMap.get('idNombreResponsable')
    var apellido_responsable = this.activerouter.snapshot.paramMap.get('idApellidoResponsable')
    return nombre_responsable + ' ' + apellido_responsable
  }

  editarEstadoTarea(id:any, nombre_tarea:any, fecha_plazo:any, fecha_asignacion:any){
    this.router.navigate(['editarEstadoTareaMiFlujo/'+ id +'/'+nombre_tarea+'/'+fecha_plazo+'/'+fecha_asignacion])
  }
  
  cargarTareasFlujo() {
    let idFecha = this.activerouter.snapshot.paramMap.get('idFecha')
    let id = this.activerouter.snapshot.paramMap.get('id')
    
    this.tyf.getTareasMiFlujo(idFecha, id).subscribe((resp: any) => {
      console.log(resp)
      var nombre_responsable = resp.find((element: { fecha_plazo: any; }) => element.fecha_plazo)
      //this.editarForm.get('fecha_plazo')?.setValue(prueba.fecha_plazo)


      // console.log(resp)
      this.listTareasFlujo = resp
      this.dataSource = new MatTableDataSource(this.listTareasFlujo)
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort
    })
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  // CAMBIAR EL ENDPOINT
  eliminarFlujosDeTrabajos(id: any) {
    Swal.fire({
      title: '¿Estas seguro?',
      text: "Esto eliminará el registro",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.tyf.deleteFlujosDeTrabajo(id).subscribe(data => {
          if (result.isConfirmed) {
            Swal.fire(
              'Eliminado!',
              'Registro correctamente eliminado',
              'success'
            )
          }
          this.cargarTareasFlujo()
        }, err => {
          let message = ""
          if (err.error.code = 23000) {
            message = "Esta Tarea tiene empleados Asociados"
          } else {
            message = "Error no controlado"
          }
          this.alertas.showFormError(message)
        })
      }
    })
  }

  /* 
  editarFlujosDeTrabajos(id:any){
    this.router.navigate(['editarFlujo/' + id])
  }

  crearTareaFlujo(id:any){
    this.router.navigate(['agregarTareaFlujo/' + id])
  }

  listarTareasFlujo(id:any){
    this.router.navigate(['listarTareasFlujo/' + id])
  }
  */



  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  verificarRolAdmin() {
    const rol_usuario = sessionStorage.getItem("rol_usuario")
    if (rol_usuario == "Administrador" || rol_usuario == "Diseñador de flujos") {
      this.isAdministrador = true
      this.isDisenador = true
    } else {
      this.alertas.errorPermisos()
    }
    return rol_usuario
  }
  //FIN configurando ROL

}
