import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarTareasMiFlujoComponent } from './listar-tareas-mi-flujo.component';

describe('ListarTareasMiFlujoComponent', () => {
  let component: ListarTareasMiFlujoComponent;
  let fixture: ComponentFixture<ListarTareasMiFlujoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarTareasMiFlujoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarTareasMiFlujoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
