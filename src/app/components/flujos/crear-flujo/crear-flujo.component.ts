import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { FlujosDeTrabajoI } from 'src/app/models/tareasYFlujos/flujosDeTrabajo.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { OrganizacionService } from 'src/app/servicios/organizacion/organizacion.service';
import { TareasYFlujosService } from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';

@Component({
  selector: 'app-crear-flujo',
  templateUrl: './crear-flujo.component.html',
  styleUrls: ['./crear-flujo.component.css']
})
export class CrearFlujoComponent implements OnInit {

  public data: Array<any> = []
  public empresas: Array<any> = []

  datosFlujosDeTrabajo!: FlujosDeTrabajoI;
  nuevoForm = this.fb.group({
    id_flujo_trabajo: new FormControl(''),
    nombre_flujo_trabajo: new FormControl(''),
    descripcion_flujo_trabajo: new FormControl(''),
    id_empresa: new FormControl(sessionStorage.getItem("id_empresa"))
  })

  constructor(private fb: FormBuilder, private activerouter: ActivatedRoute, private router: Router, private orgSvc: OrganizacionService,
    private dialogRef: MatDialog, public authService: AuthService, private alertas: AlertasService, public tyf: TareasYFlujosService) { }

  // NUEVO INTENTO DE CREAR EMPLEADO - EMPRESA

  ngOnInit(): void {
    this.obtenerData()
    this.obtenerEmpresas()
    this.authService.onCheckUser()
    this.verificarRolAdmin()
  }

  postForm(form: FlujosDeTrabajoI) {
    if (this.nuevoForm.valid) {
      this.tyf.createFlujosDeTrabajo(form).subscribe(data => {
        this.alertas.ShowSucces2()
        this.router.navigate(['/listarFlujos'])
      }, err => {
        let message = "<ul>"
        err.error.forEach((element: { field: string; message: string; }) => {
          message += "<li> " + element.message + "</li> <hr>"
        });
        message += "</ul>"
        this.alertas.showFormError(message)
        console.log("Este es una captura de error: ", err.error)
        console.log("ERRRRRRRROR", this.data)
      })
    } 
  }

  obtenerData() {
    let id= sessionStorage.getItem("id_empresa")
    this.orgSvc.getAreas(id).subscribe((resp: any) => {
      this.data = resp
      console.log(this.data)
    })
  }

  obtenerEmpresas() {
    this.orgSvc.getEmpresas().subscribe((resp: any) => {
      this.empresas = resp
      console.log(this.empresas)
    })
  }

  get nombre_flujo() { return this.nuevoForm.get('nombre_flujo'); }
  get id_empresa() { return this.nuevoForm.get('id_empresa'); }
  get descripcion_flujo() { return this.nuevoForm.get('descripcion_flujo'); }

  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  verificarRolAdmin() {
    const rol_usuario = sessionStorage.getItem("rol_usuario")
    if (rol_usuario == "Administrador" || rol_usuario == "Diseñador" ) {
      this.isAdministrador = true
      this.isDisenador = true
    } else {
      this.alertas.errorPermisos()
    }
    return rol_usuario
  }
  //FIN configurando ROL

}
