import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarTareaMiFlujoComponent } from './editar-tarea-mi-flujo.component';

describe('EditarTareaMiFlujoComponent', () => {
  let component: EditarTareaMiFlujoComponent;
  let fixture: ComponentFixture<EditarTareaMiFlujoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarTareaMiFlujoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarTareaMiFlujoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
