import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { DependenciaTareaI } from 'src/app/models/tareas/dependenciaTarea.interface';
import { TareasI } from 'src/app/models/tareasYFlujos/tareas.interface';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { TareasYFlujosService } from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-editar-tarea-mi-flujo',
  templateUrl: './editar-tarea-mi-flujo.component.html',
  styleUrls: ['./editar-tarea-mi-flujo.component.css']
})
export class EditarTareaMiFlujoComponent implements OnInit {

  constructor(private activerouter: ActivatedRoute, private router: Router, private api: TareasYFlujosService, private fb: FormBuilder,
    public authService: AuthService) { }

  public tareas: Array<any> = []

  datosAreas!: DependenciaTareaI;
  editarForm = this.fb.group({
    id_empresa: new FormControl(sessionStorage.getItem("id_empresa")),
    fecha_plazo: new FormControl(this.activerouter.snapshot.paramMap.get('fecha_plazo')),
    fecha_asignacion: new FormControl(this.activerouter.snapshot.paramMap.get('fecha_asignacion')),
    id_estado_tarea_flujo_empleado: new FormControl(''),
    id_tarea_empleado: new FormControl(''),
    nombre_tarea: new FormControl(this.activerouter.snapshot.paramMap.get('nombre_tarea'))
  })

  sucription: Subscription | undefined;
  ngOnInit(): void {
    //this.obtenerData()
    this.obtenerDataTareas()
    this.cargarEstadosTareas()
    this.verificarRolAdmin()
    this.authService.onCheckUser()

  }

  // CAMBIANDO EL ESTADO DE LA TAREA
  postForm(form: TareasI) {
    let areaid = this.activerouter.snapshot.paramMap.get('id')

    Swal.fire({
      title: '¿Quieres guardar tus cambios??',
      showDenyButton: true,
      confirmButtonText: 'Confirmar',
      denyButtonText: `Cancelar`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        Swal.fire('Cambios realizados!', '', 'success')
        this.api.putTareaMiFlujo(form, areaid).subscribe(data => {
          let respuesta: TareasI = data
          this.router.navigate(['/listarMisFlujos'])

        })
      } else if (result.isDenied) {
        Swal.fire('Los cambios no fueron guardados', '', 'info')
        this.router.navigate(['/listarMisFlujos'])
      }
    })

  }

  obtenerDataTareas() {
    let idFecha = this.activerouter.snapshot.paramMap.get('idFecha')
    let id = this.activerouter.snapshot.paramMap.get('id')

    this.api.getTareasMiFlujo(idFecha, id).subscribe((data: any) => {
      this.datosAreas = data
      this.editarForm.patchValue({
        "id_estado_tarea_flujo_empleado": this.datosAreas.id_estado_tarea_flujo_empleado,
      })

    })
  }

  public estados: Array<any> = []
  cargarEstadosTareas() {
    this.api.getEstadosTareas().subscribe((resp: any) => {
      this.estados = resp
      console.log(this.estados)
    })
  }



  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  obtenerRol() {
    const rol_usuario = sessionStorage.getItem("rol_usuario")

    return rol_usuario
  }

  verificarRolAdmin() {
    if (this.obtenerRol() == "Administrador") {
      this.isAdministrador = true
      this.isFuncionario = true
    } else {
      Swal.fire(
        'Error',
        'No cuenta con los permisos para acceder a esta funcionalidad',
        'error'
      ).then((result) => {
        this.router.navigate(['/principal'])
      });
    }
    return this.obtenerRol()
  }

}
