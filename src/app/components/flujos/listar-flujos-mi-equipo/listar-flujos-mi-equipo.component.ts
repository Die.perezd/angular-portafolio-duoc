import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { FlujoEmpleadoI } from 'src/app/models/tareasYFlujos/flujosDeTrabajo.interface';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { TareasYFlujosService } from 'src/app/servicios/tareasYFlujos/tareas-yflujos.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-listar-flujos-mi-equipo',
  templateUrl: './listar-flujos-mi-equipo.component.html',
  styleUrls: ['./listar-flujos-mi-equipo.component.css']
})
export class ListarFlujosMiEquipoComponent implements OnInit {

  public areas: Array<any> = []
  // NUEVO DATA TABLE

  // prueba new data table
  displayedColumns: string[] = ['nombre_flujo_trabajo', 'fecha_asignacion', 'fecha_plazo'
    ,'nombre_empleado_responsable', 'nombre_empleado_jefatura', 'acciones'];

  dataSource!: MatTableDataSource<any>

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  public listMisTareas: Array<FlujoEmpleadoI> = [];

  constructor(private tyf: TareasYFlujosService, private router: Router, private dialogRef: MatDialog,
    public authService: AuthService, public alertas: AlertasService, public fb: FormBuilder, private activerouter:ActivatedRoute) { }

  // ORUEBA22
  sucription: Subscription | undefined;

  ngOnInit(): void {
    this.cargarMisFlujos()
    //this.cargarEstadosTareas()
    this.verificarRolAdmin()

    this.authService.onCheckUser()

    this.sucription = this.tyf.refresh.subscribe(() => {
      this.cargarMisFlujos()
    })
  }


  cargarMisFlujos() {
    let id = this.activerouter.snapshot.paramMap.get('id')
    this.tyf.getFlujosEmpleado(id).subscribe((resp: any) => {
      // console.log(resp)
      if (Object.entries(resp).length === 0) {
        console.log("AVISO!")
        Swal.fire({
          title: 'AVISO!',
          text: "No tienes flujos cargados de momento",
          icon: 'warning',
        })
        this.router.navigate(['/principal'])
      } else {
        this.listMisTareas = resp
        /* 
        let prueba = resp.find((element: { id_estado_tarea: any; }) => element.id_estado_tarea)
        let estado = prueba.id_estado_tarea
        */
        this.dataSource = new MatTableDataSource(this.listMisTareas)
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort
      }
    })
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  listarTareasFlujo(idFecha:any, id:any, idNombreResponsable: string, idApellidoResponsable: string ){
    this.router.navigate(['/listarTareasMiFlujo/' + idFecha + '/' +id + '/'+idNombreResponsable+'/'+idApellidoResponsable])
  }


  // configurando rol y alerta 
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  verificarRolAdmin() {
    const rol_usuario = sessionStorage.getItem("rol_usuario")
    if (rol_usuario == "Administrador") {
      this.isAdministrador = true
      this.isFuncionario = true
    } else {
      this.alertas.errorPermisos()
    }
    return rol_usuario
  }

  //FIN configurando ROL

}
