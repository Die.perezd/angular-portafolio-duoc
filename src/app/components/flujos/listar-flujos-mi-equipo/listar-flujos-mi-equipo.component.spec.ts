import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarFlujosMiEquipoComponent } from './listar-flujos-mi-equipo.component';

describe('ListarFlujosMiEquipoComponent', () => {
  let component: ListarFlujosMiEquipoComponent;
  let fixture: ComponentFixture<ListarFlujosMiEquipoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListarFlujosMiEquipoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarFlujosMiEquipoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
