import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MisFlujosComponent } from './mis-flujos.component';

describe('MisFlujosComponent', () => {
  let component: MisFlujosComponent;
  let fixture: ComponentFixture<MisFlujosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MisFlujosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MisFlujosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
