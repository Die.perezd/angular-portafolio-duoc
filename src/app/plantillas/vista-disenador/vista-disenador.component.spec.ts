import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaDisenadorComponent } from './vista-disenador.component';

describe('VistaDisenadorComponent', () => {
  let component: VistaDisenadorComponent;
  let fixture: ComponentFixture<VistaDisenadorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VistaDisenadorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VistaDisenadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
