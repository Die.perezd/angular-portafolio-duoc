import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/auth/user.interface';
import { AuthService } from 'src/app/servicios/auth/auth.service';
import { EmpleadosEmpresaI } from 'src/app/models/organizacion/empleadoEmpresa.interface';

@Component({
  selector: 'app-pag-principal',
  templateUrl: './pag-principal.component.html',
  styleUrls: ['./pag-principal.component.css']
})
export class PagPrincipalComponent implements OnInit {
  public isAdmin: any = null

  constructor(public authService:AuthService) { }
  user:User | undefined

  ngOnInit(): void {
    this.user = this.authService.getCurrentUser();
    console.log(this.user)
    this.obtenerRol()
    this.verificarRolAdmin()
    this.verificarRolFuncionario()
    this.verificarRolDiseñador()
  }

  

  // configurando rol
  isAdministrador = false
  isFuncionario = false
  isDisenador = false

  obtenerRol(){
    const rol_usuario = sessionStorage.getItem("rol_usuario")

    return rol_usuario
  }

  verificarRolAdmin(){
    if(this.obtenerRol() == "Administrador"){
      this.isAdministrador = true
    }
    return "Administrador"
  }

  verificarRolFuncionario(){
    if(this.obtenerRol() == "Funcionario"){
      this.isFuncionario = true
    }

    return "Funcionario"
  }

  verificarRolDiseñador(){
    if(this.obtenerRol() == "Diseñador de flujos"){
      this.isDisenador = true
    }

    return "Diseñador"
  }


  // JUGANDO CON SIDENAV
  opened = false;
  

}
