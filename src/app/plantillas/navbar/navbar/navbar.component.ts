import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from 'src/app/servicios/auth/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isAdmin = false;
  isLogged = false;
  @Output() toggleSidenav = new EventEmitter<void>();

  constructor(private authService:AuthService) { }

  ngOnInit(): void {
    this.authService.isLogged.subscribe( (res) => (this.isLogged = res) )
  }

  onToggleSidenav(){
    this.toggleSidenav.emit();
  }

  onLogout(){
    this.authService.logout()
  }

  getNameUser(){
    let nombreUser = sessionStorage.getItem("nombre_usuario")
    return nombreUser
  }

  getRolUser(){
    let rolUser = sessionStorage.getItem("rol_usuario")
    return rolUser
  }

  getNameEmpresa(){
    return sessionStorage.getItem("nombre_empresa")
  }


}
