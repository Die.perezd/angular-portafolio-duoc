import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaFuncionarioComponent } from './vista-funcionario.component';

describe('VistaFuncionarioComponent', () => {
  let component: VistaFuncionarioComponent;
  let fixture: ComponentFixture<VistaFuncionarioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VistaFuncionarioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VistaFuncionarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
