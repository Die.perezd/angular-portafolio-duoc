import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/servicios/auth/auth.service';

@Component({
  selector: 'app-vista-funcionario',
  templateUrl: './vista-funcionario.component.html',
  styleUrls: ['./vista-funcionario.component.css']
})
export class VistaFuncionarioComponent implements OnInit {

  constructor(public authService:AuthService) { }

  ngOnInit(): void {
  }

}
