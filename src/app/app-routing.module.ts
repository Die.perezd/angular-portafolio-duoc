import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


// RUTAS GENERALES
import { PagPrincipalComponent } from './plantillas/pag-principal/pag-principal/pag-principal.component'
import { LoginComponent } from './auth/login/login.component'
import { CheckLoginGuard } from './guards/check-login.guard';


//NUEVOS COMPONENTES
import { ListarAreaComponent } from './components/organizacion/areas/listar-area/listar-area.component';
import { CrearAreaComponent } from './components/organizacion/areas/crear-area/crear-area.component';
import { EditarAreaComponent } from './components/organizacion/areas/editar-area/editar-area.component';
import { ListarUnidadComponent } from './components/organizacion/unidades/listar-unidad/listar-unidad.component';
import { CrearUnidadComponent } from './components/organizacion/unidades/crear-unidad/crear-unidad.component';
import { EditarUnidadComponent } from './components/organizacion/unidades/editar-unidad/editar-unidad.component';
import { ListarColaboradorEmpresaComponent } from './components/organizacion/colaboradoresEmpresa/listar-colaborador-empresa/listar-colaborador-empresa.component';
import { EditarColaboradorEmpresaComponent } from './components/organizacion/colaboradoresEmpresa/editar-colaborador-empresa/editar-colaborador-empresa.component';
import { CrearColaboradorEmpresaComponent } from './components/organizacion/colaboradoresEmpresa/crear-colaborador-empresa/crear-colaborador-empresa.component';
import { ListarRolComponent } from './components/organizacion/roles/listar-rol/listar-rol.component';
import { CrearRolComponent } from './components/organizacion/roles/crear-rol/crear-rol.component';
import { EditarRolComponent } from './components/organizacion/roles/editar-rol/editar-rol.component';
import { VerAreaComponent } from './components/organizacion/areas/ver-area/ver-area.component';
import { ListarTareaComponent } from './components/tareas/mantenedorTareas/listar-tarea/listar-tarea.component';
import { CrearTareaComponent } from './components/tareas/mantenedorTareas/crear-tarea/crear-tarea.component';
import { EditarTareaComponent } from './components/tareas/mantenedorTareas/editar-tarea/editar-tarea.component';
import { ListarEquipoComponent } from './components/tareas/miEquipo/listar-equipo/listar-equipo.component';
import { CrearEquipoComponent } from './components/tareas/miEquipo/crear-equipo/crear-equipo.component';
import { EditarEquipoComponent } from './components/tareas/miEquipo/editar-equipo/editar-equipo.component';
import { AsignarTareaComponent } from './components/tareas/mantenedorTareas/asignar-tarea/asignar-tarea.component';
import { ListarMisTareasComponent } from './components/tareas/misTareas/listar-mis-tareas/listar-mis-tareas.component';
import { ListarTareasEmpleadoComponent } from './components/tareas/misTareas/listar-tareas-empleado/listar-tareas-empleado.component';
import { EditarEstadoTareaComponent } from './components/tareas/editar-estado-tarea/editar-estado-tarea.component';
import { CrearSubtareaComponent } from './components/tareas/subTareas/crear-subtarea/crear-subtarea.component';
import { ListarSubTareasComponent } from './components/tareas/subTareas/listar-sub-tareas/listar-sub-tareas.component';
import { EditarSubtareaComponent } from './components/tareas/subTareas/editar-subtarea/editar-subtarea.component';
import { ListarFlujoComponent } from './components/flujos/listar-flujo/listar-flujo.component';
import { CrearFlujoComponent } from './components/flujos/crear-flujo/crear-flujo.component';
import { EditarFlujoComponent } from './components/flujos/editar-flujo/editar-flujo.component';
import { AgregarTareaFlujoComponent } from './components/flujos/agregar-tarea-flujo/agregar-tarea-flujo.component';
import { ListarTareaFlujoComponent } from './components/flujos/listar-tarea-flujo/listar-tarea-flujo.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AsignarFlujoComponent } from './components/flujos/asignar-flujo/asignar-flujo.component';
import { MisFlujosComponent } from './components/flujos/mis-flujos/mis-flujos.component';
import { ListarTareasMiFlujoComponent } from './components/flujos/listar-tareas-mi-flujo/listar-tareas-mi-flujo.component';
import { EditarTareaMiFlujoComponent } from './components/flujos/editar-tarea-mi-flujo/editar-tarea-mi-flujo.component';
import { ListarFlujosMiEquipoComponent } from './components/flujos/listar-flujos-mi-equipo/listar-flujos-mi-equipo.component';
import { DashboardTareasComponent } from './components/dashboard/dashboard-tareas/dashboard-tareas.component';


const routes: Routes = [
  {path: '', component:LoginComponent, canActivate:[CheckLoginGuard]},
  {path: 'principal', component:PagPrincipalComponent},

  // Modulo Organizacion

  // Colaboradores-Empresa
 {path:'listarColaboradoresEmpresa', component:ListarColaboradorEmpresaComponent},
 {path:'nuevoColaboradoresEmpresa', component:CrearColaboradorEmpresaComponent},
 {path:'editarColaboradoresEmpresa/:id', component:EditarColaboradorEmpresaComponent},

  // Areas
  {path:'listarAreas', component:ListarAreaComponent},
  {path:'nuevoAreas', component:CrearAreaComponent},
  {path:'editarAreas/:id', component:EditarAreaComponent},
  {path:'verArea/:id', component:VerAreaComponent},

  // Roles
  {path:'listarRoles', component:ListarRolComponent},
  {path:'nuevoRoles', component:CrearRolComponent},
  {path:'editarRoles/:id', component:EditarRolComponent},

  // Unidades
  {path:'listarUnidades', component:ListarUnidadComponent},
  {path:'nuevoUnidades', component:CrearUnidadComponent},
  {path:'editarUnidades/:id', component:EditarUnidadComponent},


  // Modulo Tareas
  // Tareas
  {path:'listarTareas', component:ListarTareaComponent},
  {path:'nuevoTareas', component:CrearTareaComponent},
  {path:'editarTareas/:id', component:EditarTareaComponent},
  {path:'asignarTareas/:id', component:AsignarTareaComponent},

  // MIS TAREAS
  {path:'listarMisTareas', component:ListarMisTareasComponent},

  // MIS FLUJOS
  {path:'listarMisFlujos', component:MisFlujosComponent},
  {path:'listarTareasMiFlujo/:idFecha/:id/:idNombreResponsable/:idApellidoResponsable', component:ListarTareasMiFlujoComponent},
  {path:'editarEstadoTareaMiFlujo/:id/:nombre_tarea/:fecha_plazo/:fecha_asignacion', component:EditarTareaMiFlujoComponent},

  // FLUJO MI EQUIPO
  {path:'listarFlujosMiEquipo/:id', component:ListarFlujosMiEquipoComponent},
  

  // LISTAR TAREAS DE MIS EMPLEADOS
  {path:'listarTareasEmpleado/:id', component:ListarTareasEmpleadoComponent},

  // Editar ESTADO TAREA y de MIS EMPLEADOS
  {path:'editarEstadoTarea/:id', component:EditarEstadoTareaComponent},

  // SUBTAREA
  {path:'crearSubTarea/:id', component:CrearSubtareaComponent},
  {path:'listarSubTarea/:id', component:ListarSubTareasComponent},
  {path:'editarSubTarea/:id/:id2', component:EditarSubtareaComponent},
  


  //EQUIPO
  {path:'listarEquipo', component:ListarEquipoComponent},
  {path:'nuevoEquipo', component:CrearEquipoComponent},
  {path:'editarEquipo/:id', component:EditarEquipoComponent},
  {path:'asignarFlujo/:id', component:AsignarFlujoComponent},

  // NUEVO MODULO PARA FLUJOS
  {path:'listarFlujos', component:ListarFlujoComponent},
  {path:'crearFlujos', component:CrearFlujoComponent},
  {path:'editarFlujo/:id', component:EditarFlujoComponent},

  // TAREAS FLUJO
  {path:'agregarTareaFlujo/:id', component:AgregarTareaFlujoComponent},
  {path:'listarTareasFlujo/:id/:flujo', component:ListarTareaFlujoComponent},

  {path:'login' , component:LoginComponent, canActivate:[CheckLoginGuard] },

  // RUTAS DASHBOARD
  {path:'dashboard', component:DashboardComponent},
  {path:'dashboardTareas', component:DashboardTareasComponent},

  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [
  // NUEVOS COMPONENTES
  PagPrincipalComponent,

  // areas
  ListarAreaComponent,
  CrearAreaComponent,
  EditarAreaComponent,
  VerAreaComponent,

  // unidad
  ListarUnidadComponent,
  CrearUnidadComponent,
  EditarUnidadComponent,

  //colaboradores
  ListarColaboradorEmpresaComponent,
  CrearColaboradorEmpresaComponent,
  EditarColaboradorEmpresaComponent,

  // roles
  ListarRolComponent,
  CrearRolComponent,
  EditarRolComponent,

  // tareas
  ListarTareaComponent,
  CrearTareaComponent,
  EditarTareaComponent,
  AsignarTareaComponent,

  //equipos
  ListarEquipoComponent,
  CrearEquipoComponent,
  EditarEquipoComponent,
  
  // MIS TAREAS
  ListarMisTareasComponent,

  // LISTAR TAREAS DE MIS EMPLEADOS
  ListarTareasEmpleadoComponent,

  // MODIFICAR ESTADO TAREAS
  EditarEstadoTareaComponent,

  // SUBTAREA
  CrearSubtareaComponent,
  ListarSubTareasComponent,
  EditarSubtareaComponent,

  // FLUJOS
  ListarFlujoComponent,
  CrearFlujoComponent,
  EditarFlujoComponent,

  // TAREAS FLUJO
  AgregarTareaFlujoComponent,
  ListarTareaFlujoComponent,

  AsignarFlujoComponent,
  MisFlujosComponent,
  ListarTareasMiFlujoComponent,
  EditarTareaMiFlujoComponent,
  ListarFlujosMiEquipoComponent,

  // DASHBOARD
  DashboardTareasComponent,
]
