import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';
import Swal from 'sweetalert2';
import { AlertasService } from '../servicios/alertas/alertas.service';
import { AuthService } from '../servicios/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AdminCheckGuard implements CanActivate {
  
  constructor(public authService:AuthService, public alertas:AlertasService){}
  
  canActivate():Observable<boolean>{
    return this.authService.isAdmin.pipe(
      take(1),
      map((isAdmin:boolean) => !isAdmin)
    )
  }

  

}
