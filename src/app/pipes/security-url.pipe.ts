import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'securityURL'
})
export class SecurityURLPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) { }

  transform(url: string) {
    var id_empresa = sessionStorage.getItem("id_empresa")
    var url = 'http://localhost/portafolio/portafolio-duoc-2021-backend/web/index.php?r=apis/dashboard/tareas-por-mes-asignacion&id_empresa=' + id_empresa
    
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

}
