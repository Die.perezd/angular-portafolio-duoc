import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { PagPrincipalComponent } from './plantillas/pag-principal/pag-principal/pag-principal.component';
import { MenuComponent } from './plantillas/menu/menu.component';
import { NavbarComponent } from './plantillas/navbar/navbar/navbar.component';


//INICIO Servicios ALERTAS
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';
import { MaterialModule } from './material.module';
import { RouterModule } from '@angular/router';
import { FooterComponent } from './plantillas/footer/footer.component';
import { LoginComponent } from './auth/login/login.component';

//FIN servicios

import {HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService } from './servicios/auth/auth.service';
import { InterceptorService } from './servicios/interceptor.service';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { VistaFuncionarioComponent } from './plantillas/vista-funcionario/vista-funcionario.component';
import { VistaDisenadorComponent } from './plantillas/vista-disenador/vista-disenador.component';
import { OrganizacionComponent } from './components/menu-principal/organizacion/organizacion.component';
import { FlujosComponent } from './components/menu-principal/flujos/flujos.component';
import { SidenavComponent } from './components/menu-principal/sidenav/sidenav.component';
import { AsignarFlujoComponent } from './components/flujos/asignar-flujo/asignar-flujo.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { MisFlujosComponent } from './components/flujos/mis-flujos/mis-flujos.component';
import { EditarTareaMiFlujoComponent } from './components/flujos/editar-tarea-mi-flujo/editar-tarea-mi-flujo.component';
import { ListarFlujosMiEquipoComponent } from './components/flujos/listar-flujos-mi-equipo/listar-flujos-mi-equipo.component';
import { DashboardTareasComponent } from './components/dashboard/dashboard-tareas/dashboard-tareas.component';
import { SecurityURLPipe } from './pipes/security-url.pipe';




@NgModule({
  declarations: [
    AppComponent,
    // Inyectando los componentes para las rutas
    routingComponents,
    PagPrincipalComponent,
    MenuComponent,
    NavbarComponent,
    FooterComponent,
    LoginComponent,
    VistaFuncionarioComponent,
    VistaDisenadorComponent,
    OrganizacionComponent,
    FlujosComponent,
    SidenavComponent,
    AsignarFlujoComponent,
    DashboardComponent,
    MisFlujosComponent,
    EditarTareaMiFlujoComponent,
    ListarFlujosMiEquipoComponent,
    DashboardTareasComponent,
    SecurityURLPipe,

  ],
  imports: [
    RouterModule.forRoot([],{
      onSameUrlNavigation: "reload"
    }),
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(),
    MaterialModule,
    RouterModule,
  ],
  providers: [AuthService , {provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true}, MatDatepickerModule, ],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
