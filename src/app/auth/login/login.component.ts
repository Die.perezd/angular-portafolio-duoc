import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/servicios/auth/auth.service';

import { FormControl, ReactiveFormsModule } from '@angular/forms';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertasService } from 'src/app/servicios/alertas/alertas.service';


var payload = new FormData();

payload.append('username', 'usuario1')
payload.append('password', 'usuario1')

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {

  //hide para password
  hide = true;

  private isValidEmail = /\S+@+\.\S+/;

  loginForm = this.fb.group({
    username: ['', Validators.required],
    password: ['', [Validators.required, Validators.minLength(5)]],
  })



  constructor(private authService: AuthService, private fb: FormBuilder, private router: Router, private alerta: AlertasService) { }

  ngOnInit(): void {
  }

  onLogin() {
    const formValue = this.loginForm.value;
    let p_username = Object.values(formValue)[0]
    let p_password = Object.values(formValue)[1]

    var data: any = new FormData()
    data.append("username", p_username)
    data.append("password", p_password)

    this.authService.login(data).subscribe(res => {
      if (this.loginForm.valid) {
        if (res) {
          
          let token:any = Object.values(formValue)[0]
          this.authService.saveToken(res.access_token)
          this.router.navigate(['principal'])
        } else {
          this.alerta.errorLogin()
          this.limpiarFormulario()
        }
      } else {
        let message = "<h2> Los campos no pueden ser vacíos </h2>"
        this.alerta.showFormError(message)
        console.log("Not Valid")
      }
    })
  }

  /*
  getErrorMessage(field:string){
    let message =""
    const formValue = this.loginForm.value;
    let p_username = Object.values(formValue)[0]
    if(this.loginForm.get(field).errors.required){
      message = "Debe ingresar un valor"
    }
    return message
  } 

  isValidField(field:string){

  } */


  limpiarFormulario() {
    this.loginForm.reset()
  }

  get username() { return this.loginForm.get('username'); }
  get password() { return this.loginForm.get('password'); }

}
